<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExamUser;
use Faker\Generator as Faker;

$factory->define(ExamUser::class, function (Faker $faker) {
    static $quetsionId = 1;
    return [
        'exam_id' => 1,
        'user_id' => 1,
        'question_id' => $quetsionId++,
        'respuesta' => $this->faker->numberBetween(1,4),

    ];
});
