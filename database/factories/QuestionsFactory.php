<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'exam_id' => 1,
        'pregunta' => $this->faker->word,
        'opcion1' => $this->faker->sentence,
        'opcion2' => $this->faker->sentence,
        'opcion3' => $this->faker->sentence,
        'opcion4' => $this->faker->sentence,
        'op_correcta' => $this->faker->numberBetween(1,4),

    ];
});
