<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->nullable();
            $table->string('name')->unique();
            $table->string('url')->default("");
            $table->string('bibliografia')->default("");
            $table->text('description')->nullable();
            $table->text('transcription')->nullable();
            $table->string('picture')->nullable();
            $table->string('picture2')->nullable();
            $table->unsignedInteger('category_id');
            $table->unsignedBigInteger('exam_id')->nullable();
            $table->timestamps();
            $table->foreign('exam_id')->references('id')->on('exam')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
