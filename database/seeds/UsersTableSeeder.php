<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'id' => 1,
            'name' => 'Test',
            'cedula' => 'A01702828',
            'email' => 'test@gmail.com',
            'role_id' => 3,
            'remember_token'=>'Jj38oT6OzAOlbVeTLnIn75riOoBhdaBFZNZ3FzT0SDTY29GvtA37TNKwnZL5',
        ]);



    }
}
