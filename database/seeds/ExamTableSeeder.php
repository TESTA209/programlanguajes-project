<?php

use Illuminate\Database\Seeder;

class ExamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exam')->insert([
            'id'=>'1',
            'nombre' => 'Examen de Introduccion',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'2',
            'nombre' => 'Examen 2',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'3',
            'nombre' => 'Examen 3',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'4',
            'nombre' => 'Examen 4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'5',
            'nombre' => 'Examen 5',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'6',
            'nombre' => 'Examen 6',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'7',
            'nombre' => 'Examen 7',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'8',
            'nombre' => 'Examen 8',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('exam')->insert([
            'id'=>'9',
            'nombre' => 'Examen 9',
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
