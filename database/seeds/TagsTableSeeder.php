<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tags')->insert([
            'id' => 1,
            'name' => 'Curso',
            'color' => '#f5365c',
            'created_at' => now(),
            'updated_at' => now()
        ]);


    }
}
