<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Admin',
            'description' => 'Rol de Super usuario, puede ver agregar, editar y eliminar cualquier cosa, ademas tiene acceso al area restringida de "bitacora" donde puede ver las entradas y cambios del rol "creador"',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Creador',
            'description' => 'Usuario con permisos para gestionar contenido de cursos y examenes',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Usuario',
            'description' => 'Usuario que puede ver cursos y examenes',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
