<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'id' => 1,
            'name' => 'Introduccion',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'1',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 1,
                'tag_id' => 1,
            ]
        );

        DB::table('items')->insert([
            'id' => 2,
            'name' => 'Proximamente',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 2,
                'tag_id' => 1,
            ]
        );

        DB::table('items')->insert([
            'id' => 3,
            'name' => 'Proximamente 3',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'3',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 3,
                'tag_id' => 1,
            ]
        );
        DB::table('items')->insert([
            'id' => 4,
            'name' => 'Proximamente 4',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'4',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 4,
                'tag_id' => 1,
            ]
        );
        DB::table('items')->insert([
            'id' => 5,
            'name' => 'Proximamente 5',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'5',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('item_tag')->insert(
            [
                'item_id' => 5,
                'tag_id' => 1,
            ]
        );

        DB::table('items')->insert([
            'id' => 6,
            'name' => 'Proximamente 6',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'6',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 6,
                'tag_id' => 1,
            ]
        );
        DB::table('items')->insert([
            'id' => 7,
            'name' => 'Proximamente 7 ',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'7',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 7,
                'tag_id' => 1,
            ]
        );
        DB::table('items')->insert([
            'id' => 8,
            'name' => 'Proximamente 8',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'8',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 8,
                'tag_id' => 1,
            ]
        );
        DB::table('items')->insert([
            'id' => 9,
            'name' => 'Proximamente 9',
            'url' => 'https://slides.com/upyougo/deck-cb88a3/embed?byline=hidden&share=hidden/embed?postMessageEvents=true',
            'bibliografia'=>',',
            'excerpt' => 'No aplica',
            'description' => 'Nos centraremos en la descripción general del curso, su duración, sus objetivos, la metodología de enseñanza. En cuanto a la materia que nos ocupa se tocarán: generalidades de la enfermedad, su fisiopatología, factores de riesgo, epidemiología y retos diagnósticos.',
            'category_id' => 1,
            'picture'=>'pictures/Mt7VT3RVVEnLICJO45mJFHPC6fZOkkg7PGnYqWe8.jpeg',
            'status' => 'published',
            'date' => now()->format('Y-m-d'),
            'show_on_homepage' => 1,
            'options' => '["0","1"]',
            'exam_id'=>'9',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('item_tag')->insert(
            [
                'item_id' => 9,
                'tag_id' => 1,
            ]
        );


    }
}
