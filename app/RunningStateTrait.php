<?php


namespace Graze\ParallelProcess;

trait RunningStateTrait
{

    protected $started = 0.0;

    protected $finished = 0.0;

    private $state = RunInterface::STATE_NOT_STARTED;


    public function getDuration()
    {
        if ($this->finished > 0) {
            return $this->finished - $this->started;
        }
        return $this->started > 0 ? microtime(true) - $this->started : 0;
    }


    protected function setStarted($time = null)
    {
        $this->started = $time ?: microtime(true);
        $this->setState(RunInterface::STATE_RUNNING);
    }


    protected function setFinished($time = null)
    {
        $this->finished = $time ?: microtime(true);
        $this->setState(RunInterface::STATE_NOT_RUNNING);
    }


    protected function getState()
    {
        return $this->state;
    }


    protected function setState($state)
    {
        $this->state = $state;
        return $this;
    }
}
