<?php


namespace Graze\ParallelProcess;

use Exception;
use Graze\ParallelProcess\Event\DispatcherInterface;
use Throwable;

interface RunInterface extends DispatcherInterface
{
    const STATE_NOT_STARTED = 0;
    const STATE_RUNNING     = 1;
    const STATE_NOT_RUNNING = 2;


    public function hasStarted();


    public function start();


    public function isSuccessful();


    public function getExceptions();


    public function isRunning();


    public function poll();


    public function getTags();


    public function getDuration();


    public function getProgress();

    public function getPriority();
}
