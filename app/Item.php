<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'excerpt', 'description', 'url', 'bibliografia', 'picture','picture2' ,'category_id', 'status', 'date', 'show_on_homepage', 'options','exam_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];

    /**
     * Get the category of the item
     *
     * @return \App\Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the tags of the item
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Get the path to the picture
     *
     * @return string
     */
    public function path()
    {
        return "/storage/{$this->picture}";
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['item_id', 'user_id', 'progress', 'indexes', 'assigned']);
    }

    public function current_user_items()
    {
        return $this->belongsToMany(User::class)->withPivot(['item_id', 'user_id', 'progress', 'indexes', 'assigned'])
            ->wherePivot('user_id', '=', Auth::id());
    }

    public function exam()
    {
        return $this->belongsTo('App\Exam');
    }

}
