<?php

namespace Graze\ParallelProcess;

interface PrioritisedInterface
{

    public function getPriority();


    public function setPriority($priority);
}
