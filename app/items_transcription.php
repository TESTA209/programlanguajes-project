<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class items_transcription extends Model
{

    protected $table = 'items_transcription';
    protected $fillable = [
        'slideNo', 'item_id', 'text'
    ];

    public function slide()
    {
        return $this->belongsTo('App\items_transcription');
    }
}
