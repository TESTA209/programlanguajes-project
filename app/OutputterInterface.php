<?php


namespace Graze\ParallelProcess;

interface OutputterInterface
{

    public function getLastMessage();
    public function getLastMessageType();
}
