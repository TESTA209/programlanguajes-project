<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Http\Requests\TagRequest;
use App\Tag;
use App\User;
use Carbon\Carbon;

class TagController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the users
     *
     * @param \App\Tag $model
     * @return \Illuminate\View\View
     */

    public function index(Tag $model)
    {
        $this->authorize('manage-items', User::class);

        return view('tags.index', ['tags' => $model->all()]);
    }

    /*
  public function index(User $model)
  {
      $this->authorize('manage-users', User::class);

      return view('tags.index', ['tags' => $model->get()]);
  }

    /**
     * Show the form for creating a new tag
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created tag in storage
     *
     * @param \App\Http\Requests\TagRequest $request
     * @param \App\Tag $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TagRequest $request, Tag $model)
    {
        $model->create($request->all());

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Crear Tag";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();
        return redirect()->route('tag.index')->withStatus(__('Tag creado con éxito.'));
    }

    /**
     * Show the form for editing the specified tag
     *
     * @param \App\Tag $tag
     * @return \Illuminate\View\View
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\TagRequest $request
     * @param \App\Tag $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TagRequest $request, Tag $tag)
    {
        $tag->update($request->all());
        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Editar tag";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('tag.index')->withStatus(__('Tag editado con éxito.'));
    }

    /**
     * Remove the specified tag from storage
     *
     * @param \App\Tag $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Tag $tag)
    {
        if (!$tag->items->isEmpty()) {
            return redirect()->route('tag.index')->withErrors(__('Este tag tiene cursos indexados y no puede ser borrado.'));
        }

        $tag->delete();

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Eliminar tag";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();
        return redirect()->route('tag.index')->withStatus(__('Tag borrado con éxito.'));
    }
}
