<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use Graze\ParallelProcess\Pool;
use Graze\ParallelProcess\PriorityPool;
use Graze\ParallelProcess\ProcessRun;
use Symfony\Component\Process\Process;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {


        $users_count = User::count();
        $Items_count = Item::count();

        $promedioPromedio = User::get();
        $Pexamenes = 0;
        $i = 0;

        foreach ($promedioPromedio as $promedio){

            $Pexamenes += $promedio->promedio;
            if(($promedio->promedio)!=0){
                $i++;
            }

        }

        if($i != 0){
            $Pexamenes = $Pexamenes/$i;
        }

        return view('pages.dashboard', ['users_count' => $users_count, 'item_count' => $Items_count,'promedio'=>$Pexamenes]);
    }
}
