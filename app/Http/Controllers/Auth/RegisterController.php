<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Item;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/dashboard';

    protected function redirectTo()
    {

        $user = User::where('cedula', '=', auth()->user()->cedula)->first();

        $selected_items = Item::all();
        if ($selected_items) {

            foreach ($selected_items as $item) {

                //Check if the user already has the item asigned
                $exists = DB::table('item_user')->where([['item_id', '=', $item], ['user_id', '=',  auth()->user()->id]])->first();
                //If it does not have it asigned, create that relation in the pivot table
                if (!$exists) {
                    $user->items()->attach($item, array('progress' => 0, 'indexes' => '[0,0,0]', 'assigned' => true));
                } else {
                    $user->items()->updateExistingPivot($item, array('assigned' => true));
                }
            }
        }
        return '/dashboard';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {


        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'cedula' => ['required', 'string', 'max:8','unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'user_type' => ['required'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'cedula' => $data['cedula'],
            'role_id' => $data['user_type'],
            'password' => Hash::make($data['password']),

        ]);


    }
}
