<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemUser;
use Illuminate\Support\Facades\Auth;

class PathController extends Controller
{
    public function index(Item $items, ItemUser $itemUser)
    {
        $user = Auth::user();
        $user_id = auth()->id();
        return view('paths.index', ['items' => $items->whereHas('users', function ($query) use ($user) {
            $query->where([['user_id', $user->id], ['assigned', true]]);
        })->with('tags', 'category',

            'current_user_items')->get(),
            'item_user'=>$itemUser->where('user_id','=',$user_id)->get(),

            'user'=>$user,

        ]);
    }
}
