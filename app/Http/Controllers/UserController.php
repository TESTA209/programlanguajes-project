<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use App\Item;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Log;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the users
     *
     * @param \App\User $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $this->authorize('manage-users', User::class);

        return view('users.index', ['users' => $model->with('role')->get()]);
    }

    /**
     * Show the form for creating a new user
     *
     * @param \App\Role $model
     * @return \Illuminate\View\View
     */
    public function create(Role $model)
    {

        return view('users.create', ['roles' => $model->get(['id', 'name'])]);
    }

    /**
     * Store a newly created user in storage
     *
     * @param \App\Http\Requests\UserRequest $request
     * @param \App\User $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        $model->create($request->merge([
            'picture' => $request->photo ? $request->photo->store('profile', 'public') : null,
            'password' => Hash::make($request->get('password'))
        ])->all());

        //Assign al items to new user
        $user = User::where('cedula', '=', $request->get('cedula'))->first();
        error_log($user->id);
        $selected_items = Item::all();
        if ($selected_items) {
            foreach ($selected_items as $item) {
                //Check if the user already has the item asigned
                $exists = DB::table('item_user')->where([['item_id', '=', $item], ['user_id', '=', $user->id]])->first();
                //If it does not have it asigned, create that relation in the pivot table
                if (!$exists) {
                    $user->items()->attach($item, array('progress' => 0, 'indexes' => '[0,0,0]', 'assigned' => true));
                } else {
                    $user->items()->updateExistingPivot($item, array('assigned' => true));
                }
            }
        }

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Crear usuario";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('user.index')->withStatus(__('Usuario creado con éxito.'));

    }

    /**
     * Show the form for editing the specified user
     *
     * @param \App\User $user
     * @param \App\Role $model
     * @return \Illuminate\View\View
     */
    public function edit(User $user, Role $model)
    {
        return view('users.edit', ['user' => $user->load('role'), 'roles' => $model->get(['id', 'name'])]);
    }

    /**
     * Update the specified user in storage
     *
     * @param \App\Http\Requests\UserRequest $request
     * @param \App\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $hasPassword = $request->get("password");
        $user->update(
            $request->merge([
                'picture' => $request->photo ? $request->photo->store('profile', 'public') : $user->picture,
                'password' => Hash::make($request->get('password'))
            ])->except([$hasPassword ? '' : 'password'])
        );

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Editar usuario";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();
        return redirect()->route('user.index')->withStatus(__('Usuario editado con éxito.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param \App\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();
        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Destruir usuario";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('user.index')->withStatus(__('Usuario borrado con éxito.'));
    }

}
