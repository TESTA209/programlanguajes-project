<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Category;
use App\Http\Requests\CategoryRequest;
use App\User;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Category::class);
    }

    /**
     * Display a listing of the categories
     *
     * @param \App\Category $model
     * @return \Illuminate\View\View
     */
    public function index(Category $model)
    {
        $this->authorize('manage-items', User::class);

        return view('categories.index', ['categories' => $model->all()]);
    }

    /**
     * Show the form for creating a new category
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created category in storage
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param \App\Category $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request, Category $model)
    {
        $model->create($request->all());

        $bitacora = new Bitacora();
        $bitacora->author = "USER ID";
        $bitacora->action = "Crear Categoria";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('category.index')->withStatus(__('Categoría creada con éxito.'));
    }

    /**
     * Show the form for editing the specified category
     *
     * @param \App\Category $category
     * @return \Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified category in storage
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param \App\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        $bitacora = new Bitacora();
        $bitacora->author = "USER ID";
        $bitacora->action = "Editar Categoria";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('category.index')->withStatus(__('Categoría editada con éxito.'));
    }

    /**
     * Remove the specified category from storage
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category)
    {
        if (!$category->items->isEmpty()) {
            return redirect()->route('category.index')->withErrors(__('Esta categoría tiene cursos indexados y no puede ser borrada.'));
        }

        $category->delete();

        $bitacora = new Bitacora();
        $bitacora->author = "USER ID";
        $bitacora->action = "Eliminar categoria";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('category.index')->withStatus(__('Categoría borrada con éxito.'));
    }
}
