<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemUser;
use App\User;
use Illuminate\Http\Request;

class EstadisticasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users_count = User::count();
        $Items_count = Item::count();
        $item_users = ItemUser::get();
        $promedioPromedio = User::get();

        $terminados = 0;

        $Pexamenes = 0;
        $i = 0;

        foreach ($promedioPromedio as $promedio){
            $Pexamenes += $promedio->promedio;
            if(($promedio->promedio)!=0){
                $i++;
            }

            $usrs = ItemUser::where('user_id',$promedio->id)->get();
            $j = 0;

            foreach ($usrs as $usr){
                if(($usr->exam_score)>= 60){
                    $j++;
                }
            }

            if($j == 8){
                $terminados++;
            }

        }

        if($i != 0){
            $Pexamenes = $Pexamenes/$i;
        }

        return view('estadisticas.index',['users_count' => $users_count, 'item_count' => $Items_count,'promedio'=>$Pexamenes,'terminados'=>$terminados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
