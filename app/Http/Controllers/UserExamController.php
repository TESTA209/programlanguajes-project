<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use Graze\ParallelProcess\Pool;
use Illuminate\Http\Request;
use App\ItemUser;
use Illuminate\Support\Facades\Auth;
use App\Question;
use App\ExamUser;
use Symfony\Component\Process\Process;

class UserExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Item $model,ItemUser $itemUser)
    {

        $user = Auth::user();
        $user_id = auth()->id();
            return view('exams_user.index', [
                'items' => $model->whereHas('users', function ($query) use ($user) {
                $query->where([['user_id', $user->id], ['assigned', true]
                ]);})->with('tags', 'category',
                'current_user_items')->get(),
                'item_user'=>$itemUser->where('user_id','=',$user_id)->get(),

            ]);
    }

    public function examen(Question $question,$exam_id,$index){

        $user = Auth::user();
        $intento = ItemUser::where('user_id','=',$user->id)->where('item_id','=',$exam_id)->first();
        $intento->exam_answered =$intento->exam_answered + 1;
        $intento->save();

        return view('exams_user.exam',['questions'=>$question->where('exam_id','=',$exam_id)->get(),'index'=>$index,'exam'=>$exam_id]);
    }

    public function subir(Request $request, Question $question,$exam_id,$index){

        $user = Auth::user();
        $user_id = auth()->id();

        $NoQuestion = $question->where('exam_id','=',$exam_id)->get();

        if( ($index + 1) >= count($NoQuestion)){
            $registro = new ExamUser;
            $registro->exam_id = $exam_id;
            $registro->user_id = $user_id;
            $registro->question_id = $request->question;
            $registro->respuesta = $request->respuesta;
            $registro->save();
            return view('exams_user.calificar',['exam' =>$exam_id]);
        }else{

            $index = $index +1;

            $registro = new ExamUser;
            $registro->exam_id = $exam_id;
            $registro->user_id = $user_id;
            $registro->question_id = $request->question;
            $registro->respuesta = $request->respuesta;
            $registro->save();


            return view('exams_user.exam',['questions'=>$question->where('exam_id','=',$exam_id)->get(),'index'=>$index,'exam'=>$exam_id]);
        }
    }
/*
 *
 *
 *
 *        !!!!! PROGRAMMING LANGUAGES PROJECT IMPELEMNTATION------------------------------------------------------
 *
 *
 *
 *
 * */
    public function calificar(Question $question ,ExamUser $examUser,$exam_id){

        // In the way we can manage multiple processor we are going to use the multi process paradigm calling it from the model
        // First iteration finding the questions from the user
        // Second Iteration finding the exam answers from the ITEM user

        $pool = new Pool();
        $pool->add(new Process($thisQUestions = $question->where('exam_id','=',$exam_id)->get()));
        $pool->add(new Process($thisExam = $examUser->where('exam_id','=',$exam_id)->get()));

        $pool->start();


        $thisQUestions = $question->where('exam_id','=',$exam_id)->get();
        $thisExam = $examUser->where('exam_id','=',$exam_id)->get();
       $total = count($thisQUestions);

       // Now we are passing this to the view
       $contador = 0;
       $correctas = 0;

       return view('exams_user.calificacion',['total'=>$total,'contador'=>0,'res_examen'=>$thisExam,'respuestasC'=>$thisQUestions,'correctas'=>$correctas,'exam'=>$exam_id]);

    }

    /*
     *
     *
     *
     *
     *
     *
     *
     *
     * */

    public function salvar_cali(Request $request,ItemUser $itemUser,$examen,$calificacion){


        $user = Auth::user();
        $user_id = auth()->id();


        $Oneid = $itemUser->where('item_id','=',$examen)->get();

        $id = $Oneid->where('user_id','=',$user_id)->first();


        $myUser = ItemUser::find($id->id);
        $myUser->exam_answered = true;
        $myUser->exam_score = $calificacion;

        $myUser->save();

        $PromedioTotal = 0;
        $i = 0;
        $promedioT = ItemUser::where('user_id','=',$user_id)->get();



        foreach ($promedioT as $promedio){
            $PromedioTotal += ($promedio->exam_score);
            if(($promedio->exam_score)!=0){
                $i++;
            }
        }


        $PromedioTotal = $PromedioTotal/$i;

        $UserPormedio = User::find($user_id);
        $UserPormedio->promedio = $PromedioTotal;
        $UserPormedio->save();


        $restQuestions = ExamUser::where('user_id','=',$user_id)->delete();

        return redirect('userExam');


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
