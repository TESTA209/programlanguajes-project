<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Http\Requests\RoleRequest;
use App\Role;
use App\User;
use Carbon\Carbon;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class);
    }

    /**
     * Display a listing of the roles
     *
     * @param \App\Role $model
     * @return \Illuminate\View\View
     */
    public function index(Role $model)
    {
        $this->authorize('manage-users', User::class);

        return view('roles.index', ['roles' => $model->all()]);
    }

    /**
     * Show the form for creating a new role
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created role in storage
     *
     * @param \App\Http\Requests\RoleRequest $request
     * @param \App\Role $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleRequest $request, Role $model)
    {
        $model->create($request->all());

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Crear Rol";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('role.index')->withStatus(__('Rol creado con éxito.'));
    }

    /**
     * Show the form for editing the specified role
     *
     * @param \App\Role $role
     * @return \Illuminate\View\View
     */
    public function edit(Role $role)
    {
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified role in storage
     *
     * @param \App\Http\Requests\RoleRequest $request
     * @param \App\Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->all());

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Editar rol";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('role.index')->withStatus(__('Rol editado con éxito.'));
    }
}
