<?php

namespace App\Http\Controllers;

use App\Exam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Question;
use Pool;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Exam $exam, Question $question,$id)
    {
        return view('preguntas.index',['questions'=>$question->where('exam_id','=',$id)->get(),'exam'=>$exam->where('id','=',$id)->get()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Exam $exam,$id)
    {
        return view('preguntas.create',['exam'=>$exam->where('id','=',$id)->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $pregunta = new Question;

        $pregunta->exam_id = $id;
        $pregunta->pregunta = $request->pregunta;
        $pregunta->opcion1 = $request->op1;
        $pregunta->opcion2 = $request->op2;
        $pregunta->opcion3 = $request->op3;
        $pregunta->opcion4 = $request->op4;
        $pregunta->op_correcta = $request->respuesta;
        $pregunta->created_at = Carbon::now();
        $pregunta->updated_at = Carbon::now();

        $pregunta->save();

        return redirect('/preguntas/'.$id)->with('status', 'Se agrego la pregunta correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question, Exam $exam,$exam_id,$id)
    {

        return view('preguntas.edit',['exam'=>$exam->where('id','=',$exam_id)->get()
        ,'pregunta'=>$question->where('id','=',$id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $exam_id, $id)
    {



        $pregunta = Question::find($id);

        $pregunta->exam_id = $exam_id;
        $pregunta->pregunta = $request->pregunta;
        $pregunta->opcion1 = $request->op1;
        $pregunta->opcion2 = $request->op2;
        $pregunta->opcion3 = $request->op3;
        $pregunta->opcion4 = $request->op4;
        $pregunta->op_correcta = $request->respuesta;
        $pregunta->updated_at = Carbon::now();

        $pregunta->save();

        return redirect('/preguntas/'.$exam_id)->with('status', 'Se edito la pregunta correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($exam_id,$id)
    {
        $pregunta = Question::find($id);
        $pregunta->delete();
        return redirect('/preguntas/'.$exam_id)->with('status', 'Se elimino la pregunta correctamente!');
    }
}
