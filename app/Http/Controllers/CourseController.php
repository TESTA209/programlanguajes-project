<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Tag;

class CourseController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Display a listing of the items
     *
     * @param \App\Item $model
     * @return \Illuminate\View\View
     */
    public function index(Item $item, Tag $tagModel, Category $categoryModel)
    {
        return view('courses.index', [
            'item' => $item->load('tags'),
            'tags' => $tagModel->get(['id', 'name']),
            'categories' => $categoryModel->get(['id', 'name'])
        ]);
    }

}
