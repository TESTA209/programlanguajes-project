<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Item;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemUserController extends Controller
{

    /**
     * Display a listing of the users
     *
     * @param \App\User $model
     * @param \App\Item $model2
     * @return \Illuminate\View\View
     */
    public function index(User $model, Item $model2)
    {
        $this->authorize('manage-users', User::class);
        return view('item_user.index', ['users' => $model->with('role')->get(), 'items' => $model2->get()]);
    }

    public function del_item_user(User $model, Item $model2)
    {
        $this->authorize('manage-users', User::class);
        return view('item_user.remove', ['users' => $model->with('role')->get(), 'items' => $model2->get()]);
    }

    /**
     * Display a listing of the items
     *
     * @param \App\Item $model
     * @return \Illuminate\View\View
     */
    public function progress(Item $model)
    {
        $user = Auth::user();
        if ($user->role_id == 1) {
            return view('item_user.progress', ['items' => $model
                ->with('tags')
                ->join('item_user', 'item_user.item_id', '=', 'items.id')
                ->join('users', 'item_user.user_id', '=', 'users.id')
                ->join('categories', 'items.category_id', '=', 'categories.id')
                ->select('users.email as user_email', 'items.name as item_name', 'item_user.*', 'categories.name as category')
                ->get()
            ]);
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(request('user_select'));
        $selected_items = request('items');
        $all_items = DB::table('items')->select('id')->get()->pluck('id')->toArray();
        $unselected_items = [];

        if ($selected_items && $all_items) {
            $unselected_items = array_diff($all_items, $selected_items);

            foreach ($selected_items as $item) {
                //Check if the user already has the item asigned
                $exists = DB::table('item_user')->where([['item_id', '=', $item], ['user_id', '=', $user->id]])->first();
                //If it does not have it asigned, create that relation int he pivot table
                if (!$exists) {
                    $user->items()->attach($item, array('progress' => 0, 'indexes' => '[0,0,0]', 'assigned' => true));
                } else {
                    $user->items()->updateExistingPivot($item, array('assigned' => true));
                }
            }

            foreach ($unselected_items as $item) {
                //Check if the user already has one of the unselected items asigned
                $exists = DB::table('item_user')->where([['item_id', '=', $item], ['user_id', '=', $user->id]])->first();
                //If it does have it asigned, update that relation an change assigned to false
                if ($exists) {
                    $user->items()->updateExistingPivot($item, array('assigned' => false));
                }
            }

            $user = auth()->user();

            $bitacora = new Bitacora();
            $bitacora->author = $user->name;
            $bitacora->action = "Asignar curso a alumno";
            $bitacora->created_at = Carbon::now();
            $bitacora->updated_at = Carbon::now();
            $bitacora->save();

            return back()->withStatus(__('Los cursos han sido asignados con éxito.'));

        } else {
            return back()->withErrors(__('No seleccionaste ningún curso para el usuario'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items_user = DB::table('item_user')
            ->join('items', 'item_user.item_id', '=', 'items.id')
            ->where([['user_id', '=', $id], ['assigned', '=', true]])
            ->get('items.id');
        $array_items_user = [];
        foreach ($items_user as $item_user) {
            $aux = json_encode($item_user);
            array_push($array_items_user, $aux);
        }
        return $array_items_user;
    }

    /**
     * Display the specified resource.
     *
     * @param int $item_id
     * @param int $progress
     * @return \Illuminate\Http\Response
     */
    public function store_progress($item_id, $progress)
    {
        $user = Auth::user();
        $user->items()->updateExistingPivot($item_id, array('progress' => $progress));
        return "success";
    }

    /**
     * Display the specified resource.
     *
     * @param int $item_id
     * @param string $indexes
     * @return \Illuminate\Http\Response
     */
    public function store_indexes($item_id, $indexes)
    {
        $user = Auth::user();
        $user->items()->updateExistingPivot($item_id, array('indexes' => $indexes));
        return "success";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
