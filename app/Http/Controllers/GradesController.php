<?php

namespace App\Http\Controllers;

use App\ItemUser;
use App\Role;
use App\User;


class GradesController extends Controller
{

    /**
     * Display a listing of the users
     *
     * @param \App\User $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {



        return view('grades.index', ['users' => $model->with('role')->get()]);
    }

    public function grades($userid, User $user, Role $model)
    {
        $examenes = ItemUser::where('user_id','=',$userid)->get();

        return view('grades.user_grade', ['user' => $user->where('id', '=', $userid)->get()->first(),'examenes'=>$examenes]);
    }

}
