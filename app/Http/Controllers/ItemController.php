<?php

namespace App\Http\Controllers;

use App\Bitacora;
use App\Category;
use App\Http\Requests\ItemRequest;
use App\Item;
use App\items_transcription;
use App\Tag;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Item::class);
    }

    /*
     * Funcion para acutlaizar una transcripcion de Slide
     */
    public function update_slide(Request $request, $itemId, $slideId)
    {

        $this->validate($request, [
            'slideNo' => 'required|integer',
            'transcription' => '',
        ]);

        $slide = items_transcription::find($slideId);
        $slide->slideNo = $request->input('slideNo');
        $slide->item_id = $itemId;
        $slide->transcription = $request->input('transcription');
        if($request->audio == null){
            $slide->audio = $request->audio_aux;
        }else{
            $slide->audio = $request->audio->store('audio', 'public');
        }

        $slide->created_at = Carbon::now();
        $slide->updated_at = Carbon::now();
        $slide->save();

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Editar transcripcion de slide";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect('/transcription/' . $itemId)->withStatus(__('Transcripcion de slide editada con éxito.'));
    }

    /*
     * Funcion para mostrar una transcripcion de slide
     */
    public function update_show($itemId, $slideId, Item $model, items_transcription $trans)
    {
        return view('items.edit_transcription_slide', ['item' => $model->with(['tags', 'category'])->where('id', '=', $itemId)->get()->first()], ['trans' => $trans->where('id', '=', $slideId)->get()->first()]);
    }

    /*
     * Funcion mostrar el conenido de todas las slides
     */
    public function slide($id, Item $model)
    {
        return view('items.item_transcription', ['item' => $model->with(['tags', 'category'])->where('id', '=', $id)->get()->first()]);
    }

    /*
     * Funcion para mostrar
     */
    public function transcription($id, Item $model, items_transcription $trans)
    {

        return view('items.transcription', ['item' => $model->with(['tags', 'category'])->where('id', '=', $id)->get()->first()],
            ['trans' => $trans->where('item_id', '=', $id)->get()]);
    }

    /**
     * Display a listing of the items
     *
     * @param \App\Item $model
     * @return \Illuminate\View\View
     */
    public function index(Item $model)
    {
        $user = Auth::user();
        if ($user->role_id == 1 || $user->role_id == 2) {
            return view('items.index', ['items' => $model->with(['tags', 'category', 'users'])->get()]);
        } else {
            return view('items.index', ['items' => $model->whereHas('users', function ($query) use ($user) {
                $query->where([['user_id', $user->id], ['assigned', true]]);
            })->with('tags', 'category', 'current_user_items')->get()]);

        }
        //return view('items.index', ['items' => $model->with(['tags', 'category'])->get()]);
    }

    /**
     * Show the form for creating a new item
     *
     * @param \App\Tag $tagModel
     * @param \App\Category $categoryModel
     * @return \Illuminate\View\View
     */
    public function create(Tag $tagModel, Category $categoryModel, Item $model)
    {
        return view('items.create', [
            'tags' => $tagModel->get(['id', 'name']),
            'categories' => $categoryModel->get(['id', 'name',])
        ]);
    }

    /**
     * Store a newly created item in storage
     *
     * @param \App\Http\Requests\ItemRequest $request
     * @param \App\Item $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ItemRequest $request, Item $model)
    {
        $item = $model->create($request->merge([
            'picture' => $request->photo->store('pictures', 'public'),
            'show_on_homepage' => $request->show_on_homepage ? 1 : 0,
            'options' => $request->options ? $request->options : null,
            'exam_id'=> 1,
            'date' => $request->date ? Carbon::parse($request->date)->format('Y-m-d') : null
        ])->all());

        $item->tags()->sync($request->get('tags'));

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Crear Presentacion";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('item.index')->withStatus(__('Curso creado con éxito.'));
    }

    /**
     * Show the form for editing the specified item
     *
     * @param \App\Item $item
     * @param \App\Tag $tagModel
     * @param \App\Category $categoryModel
     * @return \Illuminate\View\View
     */
    public function edit(Item $item, Tag $tagModel, Category $categoryModel)
    {
        return view('items.edit', [
            'item' => $item->load('tags'),
            'tags' => $tagModel->get(['id', 'name']),
            'categories' => $categoryModel->get(['id', 'name'])
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Itemuest $request
     * @param \App\Item $item
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ItemRequest $request, Item $item)
    {
        $item->update(
            $request->merge([
                'picture' => $request->photo ? $request->photo->store('pictures', 'public') : null,
                'picture2' => $request->photo2 ? $request->photo2->store('pictures', 'public') : null,
                'show_on_homepage' => $request->show_on_homepage ? 1 : 0,
                'options' => $request->options ? $request->options : null,
                'date' => $request->date ? Carbon::parse($request->date)->format('Y-m-d') : null
            ])->except([$request->hasFile('photo') ? '' : 'picture'])
        );

        $item->tags()->sync($request->get('tags'));

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Editar presentacion";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect()->route('item.index')->withStatus(__('Curso editado con éxito.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Item $item)
    {

        $delete = DB::table('item_user')->where('item_id', '=', $item->id)->delete();

        $item->delete();

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Elminar presentacion";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();


        return redirect()->route('item.index')->withStatus(__('Curso borrado con éxito.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param \App\Item $model
     * @return \Illuminate\View\View
     */
    public function present($id, Item $model, items_transcription $transcription)
    {
        $user = Auth::user();
        if ($user->role_id == 1 || $user->role_id == 2) {
            return view('items.item_presentation', ['item' => $model->with(['tags', 'category'])->where('id', '=', $id)->get()->first()], ['tanscription' => $transcription->where('item_id', '=', $id)->get()]);
        } else {
            return view('items.item_presentation', ['item' => $model->with(['tags', 'category'])->join('item_user', 'item_user.item_id', '=', 'items.id')
                ->where([
                    ['item_user.item_id', '=', $id],
                    ['item_user.user_id', '=', $user->id],
                    ['item_user.assigned', '=', true]
                ])->get()->first()],['tanscription' => $transcription->where('item_id', '=', $id)->get()]);
        }
    }

    public function create_slide_trans(Request $request, $itemId)
    {
        $this->validate($request, [
            'slideNo' => 'required|integer',
            'transcription' => '',
        ]);

        $slide = new items_transcription();
        $slide->slideNo = $request->input('slideNo');
        $slide->item_id = $itemId;
        $slide->transcription = $request->input('transcription');
        if($request->audio == null){
            $slide->audio = null;
        }else{
            $slide->audio = $request->audio->store('audio', 'public');
        }

        $slide->created_at = Carbon::now();
        $slide->updated_at = Carbon::now();
        $slide->save();

        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Crear Transcripcion ";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect('/transcription/' . $itemId)->withStatus(__('Transcripcion de slide creada con éxito.'));
    }

    public function destroy_slide_transcription($slideId, $itemId)
    {

        $slide = items_transcription::find($slideId);
        $slide->delete();
        $user = auth()->user();

        $bitacora = new Bitacora();
        $bitacora->author = $user->name;
        $bitacora->action = "Eliminar transcripcion";
        $bitacora->created_at = Carbon::now();
        $bitacora->updated_at = Carbon::now();
        $bitacora->save();

        return redirect('/transcription/' . $itemId)->withStatus(__('Transcripcion de slide eliminada con éxito.'));
    }
}
