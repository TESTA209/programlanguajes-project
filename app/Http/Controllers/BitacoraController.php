<?php

namespace App\Http\Controllers;

use App\Bitacora;

class BitacoraController extends Controller
{
    public function index(Bitacora $bitacora)
    {

        return view('bitacora.index', ['bitacora' => $bitacora->get()]);
    }


}
