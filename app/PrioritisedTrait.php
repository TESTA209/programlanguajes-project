<?php

namespace Graze\ParallelProcess;

use Graze\ParallelProcess\Event\DispatcherInterface;
use Graze\ParallelProcess\Event\PriorityChangedEvent;

trait PrioritisedTrait
{

    protected $priority;


    public function setPriority($priority)
    {
        $oldPriority = $this->priority;
        $this->priority = $priority;
        if (!($this instanceof RunInterface && $this->hasStarted())
            && method_exists($this, 'dispatch')
            && $this instanceof PrioritisedInterface) {
            $this->dispatch(PriorityChangedEvent::CHANGED, new PriorityChangedEvent($this, $priority, $oldPriority));
        }
        return $this;
    }


    public function getPriority()
    {
        return $this->priority;
    }
}
