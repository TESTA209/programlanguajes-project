<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $table = 'bitacora';

    protected $fillable = [
        'author', 'action', 'created_at', 'updated_at'
    ];

    public function bitacora_now()
    {
        return $this->belongsTo('App\Bitacora');
    }

}
