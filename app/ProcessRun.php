<?php


namespace Graze\ParallelProcess;

use Exception;
use Graze\ParallelProcess\Event\EventDispatcherTrait;
use Graze\ParallelProcess\Event\PriorityChangedEvent;
use Graze\ParallelProcess\Event\RunEvent;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Throwable;

class ProcessRun implements RunInterface, OutputterInterface, PrioritisedInterface
{
    use EventDispatcherTrait;
    use RunningStateTrait;
    use PrioritisedTrait;


    private $process;

    private $successful = false;

    private $completed = false;

    private $last = '';

    private $lastType = 'std';

    private $updateOnPoll = true;

    private $updateOnProcessOutput = true;

    private $tags;

    public function __construct(Process $process, array $tags = [], $priority = 1.0)
    {
        $this->process = $process;
        $this->tags = $tags;
        $this->priority = $priority;
    }

    protected function getEventNames()
    {
        return [
            RunEvent::STARTED,
            RunEvent::COMPLETED,
            RunEvent::FAILED,
            RunEvent::UPDATED,
            RunEvent::SUCCESSFUL,
            PriorityChangedEvent::CHANGED,
        ];
    }

    public function start()
    {
        if (!$this->process->isStarted()) {
            $this->setStarted();
            $this->dispatch(RunEvent::STARTED, new RunEvent($this));
            $this->process->start(
                function ($type, $data) {
                    $this->lastType = $type;
                    foreach (explode("\n", $data) as $line) {
                        $line = rtrim($line);
                        if (mb_strlen($line) > 0) {
                            $this->last = $line;
                            if ($this->updateOnProcessOutput) {
                                $this->dispatch(RunEvent::UPDATED, new RunEvent($this));
                            }
                        }
                    }
                }
            );
            $this->completed = false;
        }

        return $this;
    }


    public function poll()
    {
        if ($this->completed || !$this->hasStarted()) {
            return false;
        }

        if ($this->process->isRunning()) {
            if ($this->updateOnPoll) {
                $this->dispatch(RunEvent::UPDATED, new RunEvent($this));
            }
            return true;
        }

        $this->completed = true;
        $this->setFinished();

        if ($this->process->isSuccessful()) {
            $this->successful = true;
            $this->dispatch(RunEvent::SUCCESSFUL, new RunEvent($this));
        } else {
            $this->dispatch(RunEvent::FAILED, new RunEvent($this));
        }
        $this->dispatch(RunEvent::COMPLETED, new RunEvent($this));

        return false;
    }


    public function isRunning()
    {
        return $this->process->isRunning();
    }

    public function isSuccessful()
    {
        return $this->successful;
    }


    public function hasStarted()
    {
        return $this->process->isStarted();
    }


    public function getProcess()
    {
        return $this->process;
    }


    public function setUpdateOnPoll($updateOnPoll)
    {
        $this->updateOnPoll = $updateOnPoll;
        return $this;
    }


    public function isUpdateOnPoll()
    {
        return $this->updateOnPoll;
    }


    public function setUpdateOnProcessOutput($update)
    {
        $this->updateOnProcessOutput = $update;
        return $this;
    }

    public function isUpdateOnProcessOutput()
    {
        return $this->updateOnProcessOutput;
    }


    public function getTags()
    {
        return $this->tags;
    }


    public function getProgress()
    {
        return null;
    }


    public function getLastMessage()
    {
        return $this->last;
    }


    public function getLastMessageType()
    {
        return $this->lastType;
    }


    public function getExceptions()
    {
        if ($this->hasStarted() && !$this->isSuccessful()) {
            return [new ProcessFailedException($this->process)];
        }
        return [];
    }
}
