<?php

namespace App;

use App\Http\Controllers\UserExamController;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';



    // Here we call for the new function that call the on multiple process the class UserExamm with method calificar

    // We recibe the number of requests depending on the database (see cache and routing file)
    // We develop the process with the function class

    function multiscore($numberOfRequest,$question,$examUser,$exam_id){
        for ($i = 0; $i < $numberOfRequest; $i++) {
            $process = new Process('php ' . base_path('artisan') . " task {$i}");
            $process->setTimeout(0);
            $process->disableOutput();
            $process->start();
            $processes[] = $process;
        }

        // wait for above processes to complete
        while (count($processes)) {
            foreach ($processes as $i => $runningProcess) {

                UserExamController::calificar($question,$examUser,$exam_id);

                // specific process is finished, so we remove it
                if (! $runningProcess->isRunning()) {
                    unset($processes[$i]);
                }
                sleep(1);
            }
        }

    }

}
