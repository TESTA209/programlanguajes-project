<?php

namespace App\Mail;

use App\Comentarios;
use App\Item;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComentarioSend extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $Comentario = Comentarios::get()->last();
        $user_data = User::where('id',$Comentario->usuario_id)->first();
        $curso_data = Item::where('id',$Comentario->item_id)->first();


        return $this->view('mail.comentarios',['comentarios'=>$Comentario,'user'=>$user_data,'curso'=>$curso_data]);
    }
}
