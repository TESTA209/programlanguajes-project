<?php


namespace Graze\ParallelProcess;

use Graze\ParallelProcess\Event\DispatcherInterface;
use Symfony\Component\Process\Process;
use Traversable;

interface PoolInterface extends \Countable, DispatcherInterface
{
    const CHECK_INTERVAL = 0.1;


    public function add($item, array $tags = []);


    public function start();


    public function poll();


    public function run($interval = self::CHECK_INTERVAL);


    public function getAll();


    public function getWaiting();


    public function getRunning();

    public function getFinished();
}
