<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamUser extends Model
{
    protected $table = 'user_answer';
}
