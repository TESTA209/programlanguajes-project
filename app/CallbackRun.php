<?php


namespace Graze\ParallelProcess;

use Exception;
use Graze\ParallelProcess\Event\EventDispatcherTrait;
use Graze\ParallelProcess\Event\PriorityChangedEvent;
use Graze\ParallelProcess\Event\RunEvent;
use Throwable;

class CallbackRun implements RunInterface, OutputterInterface, PrioritisedInterface
{
    use EventDispatcherTrait;
    use RunningStateTrait;
    use PrioritisedTrait;


    private $callback;

    private $successful = false;

    private $tags;

    private $exception = null;

    private $last;


    public function __construct(callable $callback, array $tags = [], $priority = 1.0)
    {
        $this->callback = $callback;
        $this->tags = $tags;
        $this->priority = $priority;
    }

    protected function getEventNames()
    {
        return [
            RunEvent::STARTED,
            RunEvent::COMPLETED,
            RunEvent::SUCCESSFUL,
            RunEvent::FAILED,
            RunEvent::UPDATED,
            PriorityChangedEvent::CHANGED,
        ];
    }


    public function start()
    {
        if (!$this->hasStarted()) {
            $this->setStarted();
            $this->dispatch(RunEvent::STARTED, new RunEvent($this));
            try {
                try {
                    $output = call_user_func($this->callback);
                    $this->handleOutput($output);
                    $this->setFinished();
                    $this->successful = true;
                    $this->dispatch(RunEvent::SUCCESSFUL, new RunEvent($this));
                } catch (Exception $e) {
                    $this->setFinished();
                    $this->successful = false;
                    $this->exception = $e;
                    $this->dispatch(RunEvent::FAILED, new RunEvent($this));
                } catch (Throwable $e) {
                    $this->setFinished();
                    $this->successful = false;
                    $this->exception = $e;
                    $this->dispatch(RunEvent::FAILED, new RunEvent($this));
                }
            } finally {
                $this->dispatch(RunEvent::COMPLETED, new RunEvent($this));
            }
        }

        return $this;
    }

    private function handleOutput($output)
    {
        if (is_string($output)) {
            $output = explode("\n", $output);
        }
        if (is_array($output)) {
            foreach ($output as $line) {
                if (is_string($line)) {
                    $line = rtrim($line);
                    if (mb_strlen($line) > 0) {
                        $this->last = $line;
                        $this->dispatch(RunEvent::UPDATED, new RunEvent($this));
                    }
                }
            }
        }
    }


    public function poll()
    {
        // non async process, so it will have finished when calling start
        return false;
    }


    public function isRunning()
    {
        return false;
    }

    public function isSuccessful()
    {
        return $this->successful;
    }

    public function hasStarted()
    {
        return $this->getState() !== RunInterface::STATE_NOT_STARTED;
    }

    public function getTags()
    {
        return $this->tags;
    }


    public function getProgress()
    {
        return null;
    }


    public function getExceptions()
    {
        if ($this->exception !== null) {
            return [$this->exception];
        }
        return [];
    }


    public function getLastMessage()
    {
        return $this->last;
    }


    public function getLastMessageType()
    {
        return '';
    }
}
