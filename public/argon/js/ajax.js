$("#user_select").change(function () {
    var userId = $("select#user_select option:checked").val();
    $('#items_select option').removeAttr("selected");
    $.get(`/item_user/${userId}`)
        .done(function (data) {
            $("#items_select").find('option:selected').prop('selected', false);
            var values = [];
            for (var i = 0; i < data.length; ++i) {
                var item_user = JSON.parse(data[i]);
                $('#items_select option[value=' + item_user.id + ']').attr('selected', true);
                values.push(item_user.id);
            }
            $('#items_select').val(values);
            $('#items_select').trigger('change');
        });
});
