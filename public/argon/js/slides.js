/*
  * https://github.com/hakimel/reveal.js#api
 */

window.addEventListener('message', function (event) {
    var slide = document.getElementById('numero').innerHTML;
    var data = JSON.parse(event.data);

    if (data.namespace === 'reveal') {
        if (data.eventName === 'slidechanged') {

            var state = data.state;
            document.querySelector('iframe').contentWindow.postMessage(JSON.stringify({method: 'getProgress'}), '*');
            document.querySelector('iframe').contentWindow.postMessage(JSON.stringify({method: 'getIndices'}), '*');


        } else if (data.method === 'getProgress') {

           var progress = (data.result.toFixed(2) * 1000)/10;

           //console.log(data.state.indexh);

            //-------------------------------Devuelve el valor---------------------------------


            //console.log(user);
            document.getElementById('trans').innerHTML = "<br>"+user[data.state.indexh].transcription;
            document.getElementById('audio').innerHTML = "<audio src="+'/storage/'+user[data.state.indexh].audio+" id=\"myAudio\" controls autoplay>\n" +
                "                                                        <p>Tu navegador no implementa el elemento audio.</p>\n" +
                "                                                    </audio>";
            $('#slides_callback').text('' + progress + '%');
            //-------------------------------Devuelve el valor---------------------------------
            if (progress == 100) {
                document.getElementById("activitie").style.display = "block";
            } else {
                document.getElementById("activitie").style.display = "none";
            }


            $('#progress_percentage').attr('aria-valuenow', progress).css('width', progress + '%');
            var item_id = $('#current_item_id').text();
            $.get(`/item/present/${item_id}/${progress}`)
                .done(function (data) {
                    //Do something
                });


        } else if (data.method === 'getIndices') {
            var item_id = $('#current_item_id').text();
            var indexh = 0;
            var indexv = 0;
            var indexf = 0;
            if (data.result.h) {
                indexh = data.result.h;
            }
            if (data.result.v) {
                indexv = data.result.v;
            }
            if (data.result.f) {
                indexf = data.result.f;
            }
            var indexes = [indexh, indexv, indexf];
            var string_indexes = indexes.toString();
            $.get(`/item/present/${item_id}/indexes/${string_indexes}`)
                .done(function (data) {
                    //Do something
                });
        }
    }
});

window.addEventListener('load', function (event) {
    var str = $('#current_item_indexes').text();
    var args = str.split(',');
    document.querySelector('iframe').contentWindow.postMessage(JSON.stringify({method: 'slide', args: args}), '*');
});

/*Example of a post with message to Slides API
document.querySelector( '.get-total-slides' ).addEventListener( 'click', function( event ) {
  document.querySelector( 'iframe' ).contentWindow.postMessage( JSON.stringify({ method: 'getProgress'}), '*' );
} );
*/
