<?php

return [
    'statuses' => [
        'published' => 'Público',
        'draft' => 'Borrador',
        'archive' => 'Archivado'
    ],

    'options' => [
        0 => 'First',
        1 => 'Second',
        2 => 'Third'
    ]
];
