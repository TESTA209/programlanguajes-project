<?php

function boot()
{

    $numberOfSimultaneousRquest = Brodcast::requests();
    $numberOfSimultaneousRquest = count($numberOfSimultaneousRquest);
    $question = Brodcast::questions();
    $examUser = Brodcast::examuser();
    $exam_id = Brodcast::exam_id();

    $request = Question::multiscore($numberOfSimultaneousRquest, $question, $examUser, $exam_id);

    if ($request->app->routeAreCached()) {
        $request->loadCacheRoutes();
    } else {
        $request->loadRoutes();
        $request->app->booted(function () {
            $this->app['router']->getRoutes()->refreshNamesLookups();
            $this->app['router']->getRoutes()->refreshActionLooups();
        });
    }
}

