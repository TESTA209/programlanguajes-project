<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('welcome');

Auth::routes();

Route::get('dashboard', 'HomeController@index')->name('home');

Route::resource('item_user', 'ItemUserController', ['except' => ['show']]);
Route::get('del_item_user', 'ItemUserController@del_item_user');
Route::get('item_user/{userId}', 'ItemUserController@show');
Route::get('users_progress', 'ItemUserController@progress');

Route::get('item/present/{itemId}', 'ItemController@present');
Route::get('item/present/{itemId}/{progress}', 'ItemUserController@store_progress');
Route::get('item/present/{itemId}/indexes/{indexes}', 'ItemUserController@store_indexes');

Route::get('item/create/activ/{itemId}', 'ItemController@activ');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('exams','ExamsContoller');

    Route::get('preguntas/{id}','QuestionController@index');
    Route::get('preguntas/create/{id}','QuestionController@create');
    Route::get('preguntas/edit/{exam_id}/{id}','QuestionController@edit');
    Route::post('preguntas/store/{id}','QuestionController@store');
    Route::put('preguntas/update/{exam_id}/{id}','QuestionController@update');
    Route::delete('preguntas/delete/{exam_id}/{id}','QuestionController@destroy');

    Route::resource('userExam','UserExamController');
    Route::resource('estadisticas','EstadisticasController');
    Route::get('exam/{exam_id}/{index}','UserExamController@examen');
    Route::post('next/{exam_id}/{index}','UserExamController@subir');
    Route::post('calificar/{examid}','UserExamController@calificar');
    Route::post('salvar/{examid}/{cali}','UserExamController@salvar_cali');

    Route::resource('/sendComentarios','ComentariosController');

    Route::resource('category', 'CategoryController', ['except' => ['show']]);
    Route::resource('tag', 'TagController', ['except' => ['show']]);
    Route::resource('item', 'ItemController', ['except' => ['show']]);
    Route::resource('path', 'PathController', ['except' => ['show']]);
    Route::resource('grades', 'GradesController', ['except' => ['show']]);
    Route::get('grades/{userid}', 'GradesController@grades');
    Route::resource('curso', 'CourseController', ['except' => ['show']]);
    Route::resource('role', 'RoleController', ['except' => ['show', 'destroy']]);
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::resource('bitacora', 'BitacoraController', ['except' => ['show']]);
    Route::get('transcription/{itemId}', 'ItemController@transcription');
    Route::get('/transcription/slide/{itemId}', 'ItemController@slide');
    Route::post('/transcription/register/{itemId}', 'ItemController@create_slide_trans');
    Route::delete('/transcription/delete/{slideId}/{itemId}', 'ItemController@destroy_slide_transcription');
    Route::get('/transcription/edit/{itemId}/{slideId}', 'ItemController@update_show');
    Route::post('/transcription/update/{itemId}/{slideId}', 'ItemController@update_slide');
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);


});
