@extends('layouts.landingBase', [
    'title' => __('Bienvenido a Xofluza'),
])

<style>

    .font_title {
        color: #707070;
        font-size: 250%;
        font-weight: bold;
    }

    .font_p {
        color: #707070;
        font-size: 90%;
        font-weight: normal;
    }

    .font_subT {
        color: #707070;
        font-size: 150%;
        font-weight: bold;
    }

    .button {
        margin-left: 20%;
        width: 45%;

    }

    .imagen {
        margin-top: 2%;
        margin-left: 30%;
    }

    .textos_T1 {
        color: #FFFFFF;
        font-size: 120%;
        font-weight: bold;
        margin-left: 45%;
    }

    .textos_T2 {
        margin-top: -3%;
        color: #FFFFFF;
        font-size: 250%;
        margin-left: 35%;
        font-weight: bold;
    }


</style>

@section('content')

    <div class="row">
        <div class="col-sm-4">
            <div class="row">
                <div class="col-sm-12">
                    <img src="{{asset('argon')}}/img/brand/logo.png" width="150v" height="60v">
                    <br><br>
                    <h1 style="padding-left: 5%" class="font_title">Influenza</h1>
                    <p style="padding-left: 10%" class="font_p">
                        Siendo una de las enfermedades más
                        <br> complejas para su diagnóstico,
                        <br>hemos creado para ti este portal educativo
                        <br>especializado en <strong>influenza</strong>, con el objetivo de contribuir
                        <br>en la educación médica continua del médico.
                        <br><br>
                        Aprenderemos de la etiología, signos y síntomas,
                        <br>nuevos tratamientos innovadores y más.
                    </p>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('register') }}">
                        <button class="btn btn-primary button"><h2 style="color: white; padding-top: 5%">Crear
                                Cuenta</h2>
                        </button>
                    </a>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-sm-4">
                    <img src="{{asset('argon')}}/img/brand/influ.png" width="150vh" height="150vw">
                </div>
                <div class="col-sm-8">
                    <br>
                    <h2 class="font_subT">La Influenza ya no <br> será un problema</h2>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-12 ">
                    <img src="{{asset('argon')}}/img/brand/doctor.png" width="500vw" height="500vw" class="imagen">
                    <p class=" textos_T1">CADA VEZ MÁS CERCA</p>
                    <p class=" textos_T2">DE LA EDUCACIÓN</p>
                </div>
            </div>
        </div>

    </div>
@endsection
