@extends('layouts.app', [
    'parentSection' => 'dashboards',
    'elementName' => 'dashboard'
])
@section('content')
    <div class="container-fluid mt--6">
        <div style="margin-top: 5%;margin-left: 3%;">
            <div class="row">
                <div class="col-sm-6">
                    <br><br>
                    <h1 style="font-size: 4rem">B-AWARE</h1>
                    <p>Programa educativo de <strong>Esclerosis Múltiple</strong> y <br>
                        <strong>Neuromielitis Óptica</strong> para médicos <br>
                        de primer contacto</p>
                    <p>
                </div>
                <div class="col-sm-6 text-right">
                    <br><br>
                    <img src="{{asset('images/doctor.png')}}" alt="Imagen doctor" width="60%">
                </div>
            </div>

            <div class ="row">
                <div class ="col-sm-12">
                    <a href="/item">
                        <button class="ini-mybut">Mis cursos</button>
                    </a>
                </div>
            </div>
            <br>

        </div>

        <br>
        <!-- Footer -->
        @include('layouts.footers.auth')
    </div>
    <div class="row m-0">
        <div class="col-sm-12">
            <div style="background-image: url({{asset('images/fondo-dash.png')}})">
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="cuadro">
                            <h3 style="color: white">Primer Modulo</h3>
                            <br>
                            <a href ="/item">
                            <img src="{{asset('images/play-button.svg')}}" alt="play" width="40%">
                            </a>
                            <br>
                            <br>
                            <p>Introduccion</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="cuadro">
                            <h3 style="color: white">Proximamente</h3>
                            <br><br><br>
                            <p>Proximamente</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="cuadro">
                            <h3 style="color: white">Proximamente</h3>
                            <br><br><br>
                            <p>Proximamente</p>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
