@extends('layouts.app', [
    'title' => __('Asignar Cursos'),
    'navClass' => 'bg-default',
    'parentSection' => 'item_user-management',
    'elementName' => 'item_user-management'
])

@section('content')
    @include('forms.header', [
        'title' => __('Asignar Cursos'),
        'description' => __('Aquí puedes asignar cursos a los usuarios'),
        'class' => 'col-lg-7'
])

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-4 order-xl-2">
                <!-- Progress track -->
            </div>
            <div class="col-xl-10 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Asignar cursos') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('item_user.store') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Usuario') }}</h6>

                            @include('alerts.success')
                            @include('alerts.errors')
                            <div class="pl-lg-4">

                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="user">{{ __('Usuario') }}</label>
                                    <select class="form-control" data-toggle="select" title="Simple select"
                                            data-live-search="true" data-live-search-placeholder="Busca ..."
                                            name="user_select" id="user_select">
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->email }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="items">{{ __('Cursos') }}</label>
                                    <select class="form-control" data-toggle="select" multiple multiple="multiple"
                                            data-placeholder="Selecciona los cursos del usuario" name="items[]"
                                            id="items_select">
                                        @foreach ($items as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button type="submit"
                                            class="btn btn-success mt-4">{{ __('Asignar cursos') }}</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
