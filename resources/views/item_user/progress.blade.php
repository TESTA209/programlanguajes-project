@extends('layouts.app', [
    'title' => __('Progreso de los usuarios'),
    'parentSection' => 'users_progress-management',
    'elementName' => 'users_progress-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Progreso de los usuarios') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Progreso de los usuarios') }}</a>
            </li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Progreso de los usuarios') }}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Aquí puedes ver el progreso de los usuarios en cada uno de sus cursos.') }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class="table-responsive py-4">
                        <table class="table align-items-center table-flush" id="datatable-basic">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Usuario') }}</th>
                                <th scope="col">{{ __('Curso') }}</th>
                                @can('manage-items', App\User::class)
                                    <th scope="col">{{ __('Progreso') }}</th>
                                @endcan
                                <th scope="col">{{ __('Asignación') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->user_email }}</td>
                                    <td>{{ $item->item_name }}</td>


                                    @can('manage-items', App\User::class)
                                        <td>
                                            <div class="progress-info">
                                                <div class="progress-percentage">
                                                    <span>{{ $item->progress }}%</span>
                                                </div>
                                                @if ($item->progress < 100)
                                                    <div class="progress-label">
                                                        <span>No terminado</span>
                                                    </div>
                                                @else
                                                    <div class="progress-label">
                                                        <span style="color:#4dc96e">Terminado</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                     aria-valuenow="{{$item->progress}}" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: {{$item->progress}}%;"></div>
                                            </div>
                                        </td>
                                    @endcan

                                    @if ($item->assigned)
                                        <td>Asignado</td>
                                    @else
                                        <td>No asignado</td>
                                    @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
