@extends('layouts.app', [
    'title' => __('Editar usuario'),
    'parentSection' => 'grade-management',
    'elementName' => 'grade-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Detalle de calificaciones') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('grades.index') }}">{{ __('Detalle de usuario') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Calificaciones de ') }}{{$user->name}}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('user.index') }}"
                                   class="btn btn-sm btn-primary">{{ __('Regresar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                    @foreach($examenes as $examen)

                        <p>
                            @switch($examen->item_id)
                                @case(1):
                                Influenza
                                @break
                                @case(2):
                                Biología de los Virus y Las Enfermedades Virales
                                @break
                                @case(3):
                                Importancia de las Primeras 48 Horas
                                @break
                                @case(4):
                                Complicaciones de la Influenza
                                @break
                                @case(5):
                                Diagnóstico
                                @break
                                @case(6):
                                Tratamiento
                                @break
                                @case(7):
                                Vacunas
                                @break
                                @case(8):
                                Nuevos Tratamientos
                                @break
                            @endswitch

                            | Calificación:  {{$examen->exam_score}}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
