@extends('layouts.app', [
    'title' => __('Editar pregunta'),
    'parentSection' => 'user-management',
    'elementName' => 'exam-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Preguntas') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">{{ __('Examenes') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="/preguntas/{{$exam[0]->id }}">{{ __('Preguntas') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Nueva pregunta') }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Nueva pregunta para {{$exam[0]->nombre}}</h3>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class ="card-body">
                        <form action="/preguntas/update/{{$pregunta[0]->exam_id}}/{{$pregunta[0]->id}}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Pregunta</label>
                                    <input type="text" class="form-control" id="pregunta"
                                           placeholder="Pregunta" name="pregunta" value="{{$pregunta[0]->pregunta}}"required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">Opcion 1</label>
                                    <input type="text" class="form-control" id="op1" placeholder="Opcion No.1" name="op1"
                                           value="{{$pregunta[0]->opcion1}}"  required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">Opcion 2</label>
                                    <input type="text" class="form-control" id="op1" placeholder="Opcion No.2" name="op2"
                                           value="{{$pregunta[0]->opcion2}}" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">Opcion 3</label>
                                    <input type="text" class="form-control" id="op1" placeholder="Opcion No.3" name="op3"
                                           value="{{$pregunta[0]->opcion3}}" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">Opcion 4</label>
                                    <input type="text" class="form-control" id="op1" placeholder="Opcion No.4" name="op4"
                                           value="{{$pregunta[0]->opcion4}}" required>
                                </div>
                            </div>
                            <div class ="form-row">
                                <div class ="form-group col-md-3">
                                    <label for="inputEmail4">Respuesta</label>
                                    <input type="text" class="form-control" id="res" placeholder="Respuesta" name="respuesta"
                                           value="{{$pregunta[0]->op_correcta}}" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
