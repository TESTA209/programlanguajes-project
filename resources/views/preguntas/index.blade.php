@extends('layouts.app', [
    'title' => __('Preguntas'),
    'parentSection' => 'user-management',
    'elementName' => 'exam-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Preguntas') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">{{ __('Examenes') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Preguntas') }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Preguntas de {{$exam[0]->nombre}}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Aquí puedes consultar y editar todas las preguntas del examen.') }}
                                </p>
                            </div>
                            <div clas ="col-sm-2">
                                <a href="/preguntas/create/{{$exam[0]->id}}"><button class="btn btn-primary">Agregar</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class="table-responsive py-4">
                        <table class="table align-items-center table-flush" id="datatable-basic">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('ID') }}</th>
                                <th scope="col">{{ __('Nombre') }}</th>
                                <th scope="col">{{ __('Fecha actualizacion') }}</th>
                                <th scope="col">Ver</th>
                                <th scope="col">Eliminar</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($questions as $question)
                                <tr>
                                    <th>{{$question->id}}</th>
                                    <th>{{$question->pregunta}}</th>
                                    <th>{{$question->updated_at}}</th>
                                    <th><a href="/preguntas/edit/{{$exam[0]->id}}/{{$question->id}}"><button class="btn btn-outline-warning">ver</button></a></th>
                                    <td>
                                        <form action="/preguntas/delete/{{$question->exam_id }}/{{$question->id}}"
                                              method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger"
                                                    onclick="confirm('{{ __("¿Estás seguro de que quieres eliminar esta pregunta?") }}') ? this.parentElement.submit() : ''">
                                                {{ __('Eliminar') }}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
