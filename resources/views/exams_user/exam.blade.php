@extends('layouts.app', [
    'title' => __('Examen'),
    'parentSection' => 'laravel',
    'elementName' => 'exam-management'
])
<div style="display: none">{{$i=-1}}</div>
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examen') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Examenes') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Examenes') }}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Responde tu examen') }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                   <div class ="card-body">

                           <h4>{{$questions[$index]->pregunta}}</h4>

                           <form action="/next/{{$exam}}/{{$index}}" method="post">
                               @csrf
                               <div style="visibility: hidden;">
                               <input  name = "question" value="{{$questions[$index]->id}}">
                               </div>

                               @if(($questions[$index]->opcion1)=='N/A' and ($questions[$index]->opcion2)=='N/A' and ($questions[$index]->opcion3)=='N/A' and ($questions[$index]->opcion4)=='N/A')
                                   <input type="hidden" name="respuesta" value="1">
                                   <textarea></textarea>
                                   <br><br>
                               @else
                                   <div class="form-check">
                                       <input class="form-check-input" type="radio" name="respuesta" id="exampleRadios1" value="1">
                                       <label class="form-check-label" for="exampleRadios1">
                                           {{$questions[$index]->opcion1}}
                                       </label>
                                   </div>
                                   <div class="form-check">
                                       <input class="form-check-input" type="radio" name="respuesta" id="exampleRadios2" value="2">
                                       <label class="form-check-label" for="exampleRadios2">
                                           {{$questions[$index]->opcion2}}
                                       </label>
                                   </div>
                                   <div class="form-check">
                                       <input class="form-check-input" type="radio" name="respuesta" id="exampleRadios1" value="3">
                                       <label class="form-check-label" for="exampleRadios1">
                                           {{$questions[$index]->opcion3}}
                                       </label>
                                   </div>
                                   <div class="form-check">
                                       <input class="form-check-input" type="radio" name="respuesta" id="exampleRadios2" value="4">
                                       <label class="form-check-label" for="exampleRadios2">
                                           {{$questions[$index]->opcion4}}
                                       </label>
                                   </div>
                                   <br><br>
                               @endif
                               <button type="submit" class="btn btn-info">Siguiente</button>
                           </form>


                   </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
