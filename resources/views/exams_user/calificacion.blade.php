@extends('layouts.app', [
    'title' => __('Examen'),
    'parentSection' => 'laravel',
    'elementName' => 'exam-management'
])
<div style="display: none">{{$i=-1}}</div>
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examen') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Examenes') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Examenes') }}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Califica tu examen') }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class ="card-body">


                        <!-- Final Iteration comparing the answers with questions and asigning the correct answer-->
                        @foreach($respuestasC as $respuestasC)
                        {{$res_examen[$contador]->respuesta}}
                        @if(($respuestasC->op_correcta)== $res_examen[$contador]->respuesta )
                           <p>{{$respuestasC->pregunta}}: <strong style="color: green">Correcto</strong></p>

                                <div style="visibility: hidden">{{$correctas = $correctas +1}}</div>
                            @else

                            <p>{{$respuestasC->pregunta}}: <strong style="color: red">Incorrecto</strong></p>

                        @endif

                            <div style="visibility: hidden">{{$contador = $contador +1}}</div>

                        @endforeach

                            <h2>Tuviste: <strong> {{$correctas}} </strong>correctas de: <strong>{{$total}}</strong> preguntas</h2>
                            <h1>Tu calificacion es {{ $cali = ($correctas/$total)*100}} / 100</h1>

                            @if($cali < 80)
                                <p>Lo siento, tu califiación fue menor a 80, vuelve a intentarlo</p>
                                <form action="/salvar/{{$exam}}/{{$cali}}" method="post">
                                    @csrf
                                    <button class="btn btn-primary" type="submit">Salvar y regresar examenes</button>
                                </form>
                            @else
                                <form action="/salvar/{{$exam}}/{{$cali}}" method="post">
                                    @csrf
                                    <button class="btn btn-primary" type="submit">Salvar</button>
                                </form>

                            @endif
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
