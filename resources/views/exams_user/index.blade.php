@extends('layouts.app', [
    'title' => __('Examenes'),
    'parentSection' => 'laravel',
    'elementName' => 'exam-management'
])
<div style="display: none">{{$i=-1}}</div>
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examenes') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Examenes') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Examenes') }}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Aquí puedes ver tus examenes') }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class="table-responsive py-4">
                        <table class="table align-items-center table-flush" id="datatable-basic">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Nombre') }}</th>
                                <th scope="col">{{ __('Calificación') }}</th>
                                <th scope="col">{{ __('Intentos') }}</th>
                               <th scope="col">{{__('Tomar')}}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($items as $item)
                                <div style="display: none">{{$i=$i+1}}</div>
                                <tr>
                                    <td style="width:30%;">
                                        <div style='width:20em; overflow:hidden;'><a
                                                href="item/present/{{ $item->id }}">{{ $item->name }}</a></div>
                                    </td>
                                    <td>
                                        @if(($item_user[$i]->exam_answered)==0)
                                            <p>No presentado</p>
                                        @else
                                            {{$item_user[$i]->exam_score}}
                                        @endif

                                    </td>
                                    <td>{{$item_user[$i]->exam_answered}}</td>
                                    @if(($item_user[$i]->exam_answered)==0)
                                        <td><a href="/exam/{{$item->exam_id}}/0"><button class="btn btn-primary">Hacer examen</button></a></td>

                                    @else
                                        <td><a href="/exam/{{$item->exam_id}}/0"><button class="btn btn-primary" >Hacer examen</button></a></td>

                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush


