@extends('layouts.app', [
    'title' => __('Learning Paths'),
    'parentSection' => 'laravel',
    'elementName' => 'learning-management'
])
<div style="display: none">{{$i=-1}}</div>
<div style="display: none">{{$a=0}}</div>
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Learning Path') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('path.index') }}">{{ __('Learning Paths') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">


        @foreach($items as $item)
            <div style="display: none">{{$i=$i+1}}</div>
            <div class="card" style="margin-right: 10%">

                <div class="card-header">
                    <a href="item/present/{{ $item->id }}"><h1>{{$item->name}}</h1></a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <a href="item/present/{{ $item->id }}"><img src="{{ $item->path() }}" alt=""
                                                                        style="max-width: 200px;"></a>
                        </div>

                        <div class="col-sm-5">
                            <a href="item/present/{{ $item->id }}"><h4>{{$item->description}}</h4></a>
                        </div>
                        <div class="col-sm-2">
                            <div class="progress-info">
                                <div class="progress-percentage">
                                    <span>{{ $item->current_user_items[0]->pivot->progress }}%</span>
                                </div>
                                @if ($item->current_user_items[0]->pivot->progress < 100)
                                    <div class="progress-label">
                                        <span>No terminado</span>
                                    </div>
                                @else
                                    <div class="progress-label">
                                        <span>Terminado</span>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="/exam/{{$item->exam_id}}">
                                <h4>
                                    <li class="fa fa-stream"></li>
                                    EXAMEN DE CAPITULO
                                </h4>
                            </a>
                        </div>
                        <div class="col-sm-2">
                            @if(($item_user[$i]->exam_answered)==0)
                                <p>No presentado</p>
                            @else
                                {{$item_user[$i]->exam_score}}
                                <div style="display: none">{{$a=$a+1}}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        <div class="card" style="margin-right: 70%;">
            <div class="card-header">
                <h3>Certificado de finalizacion </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        @if(count($items) == $a && ($user->promedio)>=80)
                            <a href="{{asset('argon/img/path/certificado.pdf')}}">
                                <h3>
                                    <li class="fa fa-certificate"> Descargar certificado</li>
                                </h3>
                            </a>
                        @else
                            <h3>
                                <li class="fa fa-certificate" style="color: grey"> Descargar certificado</li>
                            </h3>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush




