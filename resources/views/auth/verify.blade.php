@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    @include('layouts.headers.guest')

    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>{{ __('Verifica tu Email') }}</small>
                        </div>
                        <div>
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('Se ha enviado un nuevo link a tu correo.') }}
                                </div>
                            @endif

                            {{ __('Antes de seguir, porfavor confirma el email en tu correo.') }}

                            @if (Route::has('verification.resend'))
                                {{ __('Si no has recibido nuestro correo') }}, <a
                                    href="{{ route('verification.resend') }}">{{ __('haz click aquí para volver a enviar') }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
