@extends('layouts.welcome_app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-sm-6 ">
                <div class="login-center">
                    <h1>BIENVENIDO</h1>
                    <br>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 text-left">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="" for="remember">
                                        {{ __('Recuerdame') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}" style="color: white">
                                        {{ __('¿Olvidaste contraseña?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-2"></div>

                        </div>

                        <div class="row form-group ">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 text-center">
                                <button type="submit" class="btn-login">
                                    {{ __('Iniciar') }}
                                </button>

                            </div>
                            <div class="col-md-4 text-center">
                                <a href="/register">
                                    <button type="button" class="btn-login">
                                        {{ __('Registrase') }}
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
