<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs  mySidenav" id="sidenav-main">
    <div class="scrollbar-inner scroll-scrollx_visible">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{ route('home') }}">
                <h2 style="color: white">EM-AWARENESS.COM</h2>
            </a>
            <br>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
            </div>
        </div>
        <div class="row" style="margin-left: 5%">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-5">
                        <img class="rounded" width="100%"
                             src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
                             alt="User picture">
                    </div>
                    <div class="col-sm-7">
                        <span class="user-name">{{ Auth::user()->name }}</span>
                        <span class="user-role">@if((Auth::user()->role_id )==1 || (Auth::user()->role_id )==3 )
                                Administrador @else Usuario @endif</span>
                        <br>
                        <span class=" user-name user-status"><i class="fa fa-circle"></i><span>En línea</span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item {{ $parentSection == 'dashboards' ? 'active' : '' }}">
                        <a class="nav-link collapsed {{ $parentSection == 'dashboards' ? 'active' : '' }}"
                           href="{{'/dashboard'}}" aria-controls="navbar-dashboards"
                           style="color: {{ $parentSection == 'dashboards' ? '#6065ae' : '' }}">

                            <span class="@if($parentSection == 'dashboards') nav-link-text2 @else nav-link-text @endif ">{{ __('General') }}</span>
                        </a>
                    </li>


{{--
                    @if((Auth::user()->id) == 1 )
                        @can('manage-items', App\User::class)
                            <li class="nav-item {{ $elementName == 'role-management' ? 'active' : '' }}">
                                <a class="nav-link collapsed {{ $elementName == 'role-management' ? 'active' : '' }}"
                                   href="{{ route('role.index') }}" aria-controls="navbar-dashboards"
                                   style="color: {{ $elementName == 'role-management' ? '#f4645f' : '' }}">
                                    <i class="fa fa-user-tag text-primary2"></i>
                                    <span class="nav-link-text">{{ __('Roles') }}</span>
                                </a>
                            </li>
                        @endcan
                    @endif
                    --}}
                    @can('manage-users', App\User::class)

                        <li class="nav-item {{ $elementName == 'user-management' || $elementName == 'users_progress-management' || $elementName == 'item_user-management'? 'active' : '' }}">
                            <a class="nav-link {{ $elementName == 'user-management' || $elementName == 'users_progress-management' || $elementName == 'item_user-management'? 'active' : '' }}"
                               style="color: {{ $elementName == 'user-management' || $elementName == 'item_user-management' ? '#f4645f' : '' }}"
                               href="#navbar-examples2" data-toggle="collapse" role="button" aria-expanded="false"
                               aria-controls="navbar-examples2">
                                <i class="fa fa-users text-primary2"></i>
                                <span class="@if($elementName == 'user-management' || $elementName == 'users_progress-management') nav-link-text2 @else nav-link-text @endif">{{ __('Alumnos') }}</span>
                            </a>
                            <div class="collapse" id="navbar-examples2">
                                <ul class="nav nav-sm flex-column">
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'user-management' ? 'active' : '' }}"
                                            href="{{ route('user.index') }}" aria-controls="navbar-dashboards"
                                            style="color: {{ $elementName == 'user-management' ? '#f4645f' : '' }}">
                                            <a href="{{ route('user.index') }}"
                                               class="nav-link {{ $elementName == 'user-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Administrar Usuarios/Alumnos') }}</a>
                                        </li>
                                    @endcan
                                    {{--
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'item_user-management' ? 'active' : '' }}">
                                            <a href="{{'/item_user'}}"
                                               class="nav-link {{ $elementName == 'item_user-management' ? 'active' : '' }}"
                                               style="color: white"">{{ __('Asignar Cursos') }}</a>
                                        </li>
                                    @endcan
                                    --}}
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'users_progress-management' ? 'active' : '' }}">
                                            <a href="{{'/users_progress'}}"
                                               class="nav-link {{ $elementName == 'users_progress-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Progresos') }}</a>
                                        </li>
                                    @endcan
                                </ul>
                            </div>


                        </li>

                    @endcan
                    <li class="nav-item {{ $parentSection == 'laravel' ? 'active' : '' }}">
                        <a class="nav-link {{ $parentSection == 'laravel' ? 'active' : '' }}"
                           style="color: {{ $parentSection == 'laravel' ? '#f4645f' : '' }}" href="#navbar-examples"
                           data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                            <i class="fa fa-scroll text-primary2"></i>
                            <span class="@if($elementName == 'item-management') nav-link-text2 @else nav-link-text @endif">{{ __('Cursos') }}</span>
                        </a>
                        <div class="collapse" id="navbar-examples">
                            <ul class="nav nav-sm flex-column">
                                @can('manage-items', App\User::class)
                                    <li class="nav-item {{ $elementName == 'item-management' ? 'active' : '' }}">
                                        <a href="{{ route('item.index') }}"
                                           class="nav-link {{ $elementName == 'item-management' ? 'active' : '' }}"
                                           style="color: white">{{ __('Administrar Cursos') }}</a>
                                    </li>

                                @endcan
                                {{--
                                @if((Auth::user()->id) == 1 )
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'category-management' ? 'active' : '' }}">
                                            <a href="{{ route('category.index') }}"
                                               class="nav-link {{ $elementName == 'category-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Administrar Categorías') }}</a>
                                        </li>
                                    @endcan
                                @endif
                                --}}

                                @can('manage-items', App\User::class)
                                    {{--
                                    @if((Auth::user()->id) == 1 )
                                        <li class="nav-item {{ $elementName == 'tag-management' ? 'active' : '' }}">
                                            <a href="{{ route('tag.index') }}"
                                               class="nav-link {{ $elementName == 'tag-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Administrar Tags') }}</a>
                                        </li>
                                            <li class="nav-item {{ $elementName == 'item-management' ? 'active' : '' }}">
                                                <a href="{{ route('item.index') }}"
                                                   class="nav-link {{ $elementName == 'item-management' ? 'active' : '' }}"
                                                   style="color: white">{{ __('Cursos') }}</a>
                                            </li>
                                    @endif
                                    --}}
                                @else
                                    <li class="nav-item {{ $elementName == 'item-management' ? 'active' : '' }}">
                                        <a href="{{ route('item.index') }}"
                                           class="nav-link {{ $elementName == 'item-management' ? 'active' : '' }}"
                                           style="color: white">{{ __('Cursos') }}</a>
                                    </li>
                                    <li class="nav-item {{ $elementName == 'exam-management' ? 'active' : '' }}">
                                        <a href="{{route('userExam.index')}}"
                                           class="nav-link {{ $elementName == 'exam-management' ? 'active' : '' }}"
                                           style="color: white">{{ __('Examenes') }}</a>
                                    </li>
                                    {{--
                                    <li class="nav-item {{ $elementName == 'learning-management' ? 'active' : '' }}">
                                        <a href="{{route('path.index')}}"
                                           class="nav-link {{ $elementName == 'learning-management' ? 'active' : '' }}"
                                           style="color: white">{{ __('Learning Path') }}</a>
                                    </li>
                                    --}}
                                @endcan

                            </ul>
                        </div>
                    </li>

                    @can('manage-items', App\User::class)

                        <li class="nav-item {{ $elementName == 'exam-management' || $elementName == 'grades-management'? 'active' : '' }}">
                            <a class="nav-link {{ $elementName == 'exam-management' || $elementName == 'grades-management'? 'active' : '' }}"
                               style="color: {{ $elementName == 'exam-management' || $elementName == 'grades-management' ? '#f4645f' : '' }}"
                               href="#navbar-examples3" data-toggle="collapse" role="button" aria-expanded="false"
                               aria-controls="navbar-examples3">
                                <i class="fa fa-graduation-cap text-primary2"></i>
                                <span class="@if($elementName == 'exam-management' || $elementName == 'grades-management') nav-link-text2 @else nav-link-text @endif">{{ __('Exámenes') }}</span>
                            </a>
                            <div class="collapse" id="navbar-examples3">
                                <ul class="nav nav-sm flex-column">
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'exam-management' ? 'active' : '' }}"
                                            href="{{ route('user.index') }}" aria-controls="navbar-dashboards"
                                            style="color: white">
                                            <a href="{{route('exams.index')}}"
                                               class="nav-link {{ $elementName == 'exam-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Administrar Examenes') }}</a>
                                        </li>

                                    @endcan
                                    @can('manage-items', App\User::class)
                                        <li class="nav-item {{ $elementName == 'grades-management' ? 'active' : '' }}"
                                            href="{{ route('user.index') }}" aria-controls="navbar-dashboards"
                                            style="color: white">
                                            <a href="{{ route('grades.index') }}"
                                               class="nav-link {{ $elementName == 'grades-management' ? 'active' : '' }}"
                                               style="color: white">{{ __('Calificaciones') }}</a>
                                        </li>

                                    @endcan
                                </ul>
                            </div>


                        </li>

                    @endcan

                    @can( 'manage-users',App\User::class)
                        <li class="nav-item {{ $elementName == 'bitacora-management' ? 'active' : '' }}">
                            <a class="nav-link collapsed {{ $elementName == 'bitacora-management' ? 'active' : '' }}"
                               href="{{route('bitacora.index')}}" aria-controls="navbar-dashboards"
                               style="color: {{ $elementName == 'bitacora-management' ? '#f4645f' : '' }}">
                                <i class="fa fa-book text-primary2"></i>
                                <span class="@if($elementName == 'bitacora-management') nav-link-text2 @else nav-link-text @endif">{{ __('Bitácora') }}</span>
                            </a>
                        </li>
                    @endcan
                    @can( 'manage-users',App\User::class)
                        <li class="nav-item {{ $elementName == 'bitacora-management' ? 'active' : '' }}">
                            <a class="nav-link collapsed {{ $elementName == 'bitacora-management' ? 'active' : '' }}"
                               href="{{route('estadisticas.index')}}" aria-controls="navbar-dashboards"
                               style="color: {{ $elementName == 'bitacora-management' ? '#f4645f' : '' }}">
                                <i class="fa fa-book text-primary2"></i>
                                <span class="@if($elementName == 'bitacora-management') nav-link-text2 @else nav-link-text @endif">{{ __('Estadisticas') }}</span>
                            </a>
                        </li>
                    @endcan

                    <li class="nav-item {{ $elementName == 'profile' ? 'active' : '' }}">
                        <a class="nav-link collapsed {{ $elementName == 'profile' ? 'active' : '' }}"
                           href="{{ route('profile.edit') }}" aria-controls="navbar-dashboards"
                           style="color: {{ $elementName == 'profile' ? '#f4645f' : '' }}">
                            <i class="fa fa-user-alt text-primary2"></i>
                            <span class="@if($elementName == 'profile') nav-link-text2 @else nav-link-text @endif">{{ __('Perfil') }}</span>
                        </a>
                    </li>
                </ul>
                <!-- Divider -->
            </div>
        </div>
    </div>
</nav>
