@extends('layouts.app', [
    'title' => __('Transcripcion'),
    'parentSection' => 'laravl',
    'elementName' => 'item-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Transcripcion') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Transcripcion') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ $item->name }}</h3>
                            </div>
                            <div class="col-2 text-right">
                                <a href="/transcription/slide/{{ $item->id }}"
                                   class="btn btn-sm btn-info">{{ __('Agregar') }}</a>
                            </div>
                            <div class="col-2 text-right">
                                <a href="/item"
                                   class="btn btn-sm btn-primary">{{ __('Regresar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table align-items-center table-flush" id="datatable-basic">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Slide') }}</th>
                                <th scope="col">{{ __('Transcripcion') }}</th>
                                <th scope="col">{{ __('Fecha de actualizacion') }}</th>
                                <th scope="col"> Editar</th>
                                <th scope="col"> Eliminar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trans as $transcription)
                                <tr>
                                    <td>{{ $transcription->slideNo }}</td>
                                    <td style="width:30%;">
                                        <div style='width:15em; overflow:hidden;'><a
                                                href="/transcription/edit/{{$item->id}}/{{$transcription->id}}">{{ $transcription->transcription }}</a>
                                        </div>
                                    </td>
                                    <td>{{ $transcription->updated_at->format('d/m/Y H:i') }}</td>
                                    <td>
                                        <a href="/transcription/edit/{{$item->id}}/{{$transcription->id}}">
                                            <button class="btn btn-outline-warning">Editar</button>
                                        </a>
                                    </td>

                                    <td> <form action="/transcription/delete/{{$transcription->id}}/{{ $item->id }}"
                                               method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="button" class="btn btn-danger"
                                                    onclick="confirm('{{ __("¿Estás seguro de que quieres eliminar este curso?") }}') ? this.parentElement.submit() : ''">
                                                {{ __('Eliminar') }}
                                            </button>
                                            </form>

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br><br>

                </div>
            </div>


        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/quill/dist/quill.core.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/quill/dist/quill.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('argon') }}/js/items.js"></script>
@endpush
