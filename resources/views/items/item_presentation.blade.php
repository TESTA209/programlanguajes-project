@extends('layouts.app', [
    'title' => __('Curso'),
    'parentSection' => 'laravel',
    'elementName' => 'item-management'
])

@if (session('status'))
    <div class="alert alert-success text-right">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<style>
    #form {
        width: 250px;
        margin: 0 auto;
        height: 50px;
    }

    #form p {
        text-align: center;
    }

    #form label {
        font-size: 20px;
    }

    input[type="radio"] {
        display: none;
    }

    label {
        color: grey;
    }

    .clasificacion {
        direction: rtl;
        unicode-bidi: bidi-override;
    }

    label:hover,
    label:hover ~ label {
        color: orange;
    }

    input[type="radio"]:checked ~ label {
        color: orange;
    }
</style>


@section('content')
    <div style="visibility: hidden">{{$i=0}}</div>
    <br><br><br><br>

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h1 style="font-size: 3rem" class="mb-0">{{ $item->name }}</h1>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('item.index') }}"
                                   class="btn btn-sm btn-primary">{{ __('Regresar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-9 order-xl-1">
                                <div class="reveal">
                                    <iframe
                                        src="{{ $item->url }}"
                                        width="100%" height="500rem" scrolling="no" frameborder="0"
                                        webkitallowfullscreen mozallowfullscreen allowfullscreen>
                                    </iframe>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-header">
                                            <h1>Módulo - {{$item->name}}</h1>
                                        </div>

                                        <span id="numero"></span>
                                        <div class="card-body">
                                            @if(empty($tanscription[$i]))
                                                <p>No hay transcripcion</p>
                                            @else
                                                <div id="trans">
                                                </div>

                                                <div id="audio">
                                                </div>
                                                {{--@foreach($tanscription as $trans)
                                                    {{$trans->transcription}}
                                                    <br><br>
                                                @endforeach
                                                --}}
                                            @endif
                                        </div>

                                        <br>


                                    </div>
                                </div>
                                <!--Item id (hidden) used to know which item_user row to be modified to save progress-->
                                @cannot('manage-items', App\User::class)
                                    <span style="visibility: hidden" id="current_item_id">{{ $item->item_id }}</span>
                                @endcannot

                                @can('manage-items', App\User::class)
                                    <span style="visibility: hidden" id="current_item_id">{{ $item->id }}</span>
                                @endcan
                            <!--Item id (hidden) used to know which item_user row to be modified to save progress-->
                                <span style="visibility: hidden" id="current_item_indexes">{{ $item->indexes }}</span>

                                <span style="visibility: hidden" id="current_item_indexes">{{ $item->indexes }}</span>

                            </div>
                            <div class="col-xl-3 order-xl-1" >
                                <div class="card" style="background: #6065AE;color: white;border-radius: 15px;">
                                    <!-- Card image -->

                                    <!-- Card body -->
                                    <div class="card-body text-left">
                                        <div class="row">
                                            <div class="col-sm-12 text-center" >
                                                <img width="100%" src="{{ asset('storage') }}/{{$item->picture2}}"
                                                     alt="Imagen">

                                            </div>
                                        </div>

                                        <br><br>
                                        <div class="controls">

                                            <!--------------PROGRESSS ---------------------->
                                            <div class="progress">
                                                <div class="progress-bar bg-info" id="progress_percentage"
                                                     role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <div class="progress-info">
                                                <div class="progress-percentage">
                                                    <span id="slides_callback" style="color: white"></span>
                                                </div>
                                            </div>
                                            <!--------------PROGRESSS ---------------------->
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 text-center">
                                                <div id="activitie" style="display: none"><a href="/exam/{{$item->exam_id}}/0">
                                                        <button class="btn btn-default">Hacer examen</button>
                                                    </a></div>
                                            </div>
                                        </div>

                                        <br>
                                        <h2 style="color: white">Sobre este Módulo</h2>
                                        <p class="card-text mt-4">{!!$item->description !!}</p>
                                        <br>
                                        <br>
                                        <h2 style="color: white">Mándanos tus dudas y comentarios!</h2>
                                        <form method="POST" action="{{route('sendComentarios.store')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <style>
                                                        .Thecontainer {
                                                            max-width: 820px;
                                                            margin: 0px auto;
                                                            margin-top: 50px;
                                                        }

                                                        .comment {
                                                            float: left;
                                                            width: 100%;
                                                            height: auto;
                                                        }

                                                        .commenter {
                                                            float: left;
                                                        }

                                                        .commenter img {
                                                            width: 35px;
                                                            height: 35px;
                                                        }

                                                        .comment-text-area {
                                                            float: left;
                                                            width: 100%;
                                                            height: auto;
                                                        }

                                                        .textinput {
                                                            float: left;
                                                            width: 100%;
                                                            min-height: 75px;
                                                            outline: none;
                                                            resize: none;
                                                            border: 1px solid grey;
                                                        }
                                                    </style>
                                                    <div class="Thecontainer">
                                                        <div class="comment">
                                                            <textarea class="textinput" placeholder="Dudas y comentarios" name="Comentario"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="hidden" value="{{$item->id}}" name ="item_id">
                                                    <button class="btn btn-success" type="submit">Enviar</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/quill/dist/quill.core.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/quill/dist/quill.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('argon') }}/js/items.js"></script>

    <script>
        var i = 0;
        var user = @json($tanscription);

        var MySlide = document.getElementById('numero').innerHTML;
        document.getElementById('trans').innerHTML = "<br>"+user[i].transcription;
        document.getElementById('audio').innerHTML = "<audio src="+'/storage/'+user[i].audio +" id=\"myAudio\" controls autoplay>\n" +
            "                                                        <p>Tu navegador no implementa el elemento audio.</p>\n" +
            "                                                    </audio>";



    </script>



@endpush
