@extends('layouts.app', [
    'title' => __('Transcripcion / Slide'),
    'parentSection' => 'laravl',
    'elementName' => 'item-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Transcripcion de slide') }}
            @endslot

            <li class="breadcrumb-item"><a href="">{{ __('Item Transcription') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ $item->name }}</h3>
                            </div>
                            <div class="col-2 text-right">
                                <a href="/transcription/{{$item->id}}"
                                   class="btn btn-sm btn-primary">{{ __('Regresar') }}</a>
                            </div>
                        </div>


                    </div>
                    <div class="card-body">
                        <br>
                        <form method="post" class="item-form" action="/transcription/register/{{ $item->id }}"
                              autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('slideNo') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Slide') }}</label>
                                        <input type="text" name="slideNo" id="input-name"
                                               class="form-control{{ $errors->has('slideNo') ? ' is-invalid' : '' }}"
                                               placeholder="{{ __('No de Slide') }}" value="{{ old('slideNo') }}"
                                               required
                                               autofocus maxlength="1500">
                                        @include('alerts.feedback', ['field' => 'slide'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group{{ $errors->has('transcription') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Texto') }}</label>
                                        <input type="text" name="transcription" id="input-name"
                                               class="form-control{{ $errors->has('transcription') ? ' is-invalid' : '' }}"
                                               placeholder="{{ __('Texto') }}" value="{{ old('transcription') }}"
                                               autofocus required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                                <label class="form-control-label"
                                       for="input-picture">{{ __('Presentación') }}</label>
                                <div class="custom-file">
                                    <input type="file" name="photo"
                                           class="custom-file-input{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                                           id="input-picture" accept="image/*" required>
                                    <label class="custom-file-label"
                                           for="input-picture">{{ __('Selecciona una presentación') }}</label>
                                </div>

                                @include('alerts.feedback', ['field' => 'photo'])
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                            </div>
                        </form>
                    </div>
                    <br><br>

                </div>
            </div>


        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/quill/dist/quill.core.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/quill/dist/quill.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('argon') }}/js/items.js"></script>
@endpush
