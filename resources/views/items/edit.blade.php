@extends('layouts.app', [
    'title' => __('Editar curso'),
    'parentSection' => 'laravel',
    'elementName' => 'item-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Editar curso') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('item.index') }}">{{ __('Editar') }}</a></li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Editar curso') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('item.index') }}"
                                   class="btn btn-sm btn-primary">{{ __('Regresar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" class="item-form" action="{{ route('item.update', $item) }}"
                              autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <h6 class="heading-small text-muted mb-4">{{ __('Información del curso') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="name" id="input-name"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Nombre') }}" value="{{ old('name', $item->name) }}"
                                           autofocus>

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                <div class="form-group{{ $errors->has('category_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-role">{{ __('Categoría') }}</label>
                                    <select name="category_id" id="input-role"
                                            class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('Categoria') }}">
                                        <option value="">-</option>
                                        @foreach ($categories as $category)
                                            <option
                                                value="{{ $category->id }}" {{ $category->id == old('category_id', $item->category_id) ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    @include('alerts.feedback', ['field' => 'category_id'])
                                </div>
                                <div class="form-group{{ $errors->has('excerpt') ? ' has-danger' : '' }}"  style="display: none">
                                    <label class="form-control-label" for="input-excerpt">{{ __('Extracto') }}</label>
                                    <textarea name="excerpt" id="input-excerpt" cols="30" rows="2"
                                              class="form-control{{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                              placeholder="{{ __('Excerpt') }}"
                                              value="{{ old('excerpt') }}">{{ old('excerpt', $item->excerpt) }}</textarea>

                                    @include('alerts.feedback', ['field' => 'excerpt'])
                                </div>
                                <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Imagen portada') }}</label>
                                    <div class="custom-file">
                                        <input type="file" name="photo"
                                               class="custom-file-input{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                                               id="input-picture" accept="image/*">
                                        <label class="custom-file-label"
                                               for="input-picture">{{ __('Selecciona una imagen') }}</label>
                                    </div>

                                    @include('alerts.feedback', ['field' => 'photo'])
                                </div>

                            <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">{{ __('Imagen derecha') }}</label>
                                <div class="custom-file">
                                    <input type="file" name="photo2"
                                           class="custom-file-input{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                                           id="input-picture" accept="image/*">
                                    <label class="custom-file-label"
                                           for="input-picture">{{ __('Selecciona una imagen') }}</label>
                                </div>

                                @include('alerts.feedback', ['field' => 'photo'])
                            </div>
                                <div class="form-group{{ $errors->has('excerpt') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-excerpt">{{ __('URL') }} </label>
                                    <span style="font-size: 0.7em; color: #9ea6ad;"> - debe de empezar con
                                      <strong>https://</strong>
                                      y debe de terminar con:
                                      <strong>/embed?postMessageEvents=true</strong>
                                      <br>
                                      Ejemplo: https://slides.com/upyougo/abc-influenza/embed?postMessageEvents=true</span>
                                    <textarea name="url" id="input-excerpt" cols="30" rows="2"
                                              class="form-control{{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                              placeholder="{{ __('URL') }}"
                                              value="{{ old('excerpt') }}">{{ old('url', $item->url) }}</textarea>

                                    @include('alerts.feedback', ['field' => 'excerpt'])
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label">{{ __('Descripción') }}</label>
                                    <div data-toggle="quill" class="editor-description"
                                         data-quill-placeholder="Descripcion">
                                        {!! old('description', $item->description) !!}
                                    </div>
                                    @include('alerts.feedback', ['field' => 'description'])
                                </div>
                                <div class="form-group{{ $errors->has('tags') ? ' has-danger' : '' }}" style="display: none">
                                    <label class="form-control-label" for="input-role">{{ __('Tags') }}</label>
                                    <select name="tags[]" id="input-role"
                                            class="form-control select2{{ $errors->has('tags') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('Tags') }}" data-toggle="select" multiple>
                                        @foreach ($tags as $tag)
                                            <option
                                                value="{{ $tag->id }}" {{ in_array($tag->id, old('tags', $item->tags->pluck('id')->toArray()) ?? []) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                        @endforeach
                                    </select>

                                    @include('alerts.feedback', ['field' => 'tags'])
                                </div>
                                <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}" style="display: none">
                                    <label class="form-control-label" for="input-role">{{ __('Estatus') }}</label>
                                    @foreach (config('items.statuses') as $value => $status)
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="status" class="custom-control-input" id="{{ $value }}"
                                                   value="{{ $value }}"
                                                   type="radio" {{ old('status', $item->status) == $value ? ' checked=checked' : '' }}>
                                            <label class="custom-control-label" for="{{ $value }}">{{ $status }}</label>
                                        </div>
                                    @endforeach

                                    @include('alerts.feedback', ['field' => 'status'])
                                </div>
                                <div class="form-group" style="display: none">
                                    <label class="form-control-label" for="input-role">{{ __('Activar') }}</label>
                                    <div class="custom-field">
                                        <label class="custom-toggle">
                                            <input name="show_on_homepage" type="checkbox"
                                                   value="1" {{ old('show_on_homepage', $item->show_on_homepage) ? ' checked=checked' : ''}}>
                                            <span class="custom-toggle-slider rounded-circle" data-label-off="No"
                                                  data-label-on="Si"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('bibliografia') ? ' has-danger' : '' }}" style="display: none">
                                    <label class="form-control-label"
                                           for="input-bibliografia">{{ __('Bibliografia') }}</label>
                                    <textarea name="bibliografia" id="input-bibliografia" cols="30" rows="2"
                                              class="form-control{{ $errors->has('bibliografia') ? ' is-invalid' : '' }}"
                                              placeholder="{{ __('Bibliografia') }}"
                                              value="{{ old('bibliografia') }}">{{ old('bibliografia',$item->bibliografia) }}</textarea>

                                    @include('alerts.feedback', ['field' => 'bibliografia'])
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="date">Fecha</label>
                                            <input class="form-control datepicker" name="date" id="date"
                                                   placeholder="Select date" type="text" data-date-format="dd-mm-yyyy"
                                                   value="{{ old('date', $item->date
                                            ? \Carbon\Carbon::parse($item->date)->format('d-m-Y') : '')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Editar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/quill/dist/quill.core.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/quill/dist/quill.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('argon') }}/js/items.js"></script>
@endpush
