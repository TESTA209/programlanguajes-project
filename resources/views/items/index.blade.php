@extends('layouts.app', [
    'title' => __('Cursos'),
    'parentSection' => 'laravel',
    'elementName' => 'item-management'
])

@section('content')

    @if (auth()->user()->can('create', App\Item::class))
        <br><br><br><br><br>
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Cursos') }}</h3>
                                    <p class="text-sm mb-0">
                                        {{ __('Aquí puedes agregar y editar tus cursos.') }}
                                    </p>
                                </div>
                                @if (auth()->user()->can('create', App\Item::class))
                                    <div class="col-4 text-right">
                                        <a href="{{ route('item.create') }}"
                                           class="btn btn-sm btn-primary">{{ __('Agregar curso') }}</a>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>

                        <div class="table-responsive py-4">
                            <table class="table align-items-center table-flush" id="datatable-basic">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nombre') }}</th>
                                    <th scope="col">{{ __('Categoría') }}</th>
                                    @cannot('manage-items', App\User::class)
                                        <th scope="col">{{ __('Progreso') }}</th>
                                    @endcannot
                                    <th scope="col">{{ __('Imagen') }}</th>
                                    @can('manage-items', App\User::class)
                                        <th scope="col">{{ __('Ultima modificación') }}</th>
                                    @endcan
                                    @can('manage-items', App\User::class)
                                        <th scope="col"></th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td style="width:30%;">
                                            <div style='width:20em; overflow:hidden;'><a
                                                    href="item/present/{{ $item->id }}">{{ $item->name }}</a></div>
                                        </td>
                                        <td>{{ $item->category->name }}</td>

                                        @cannot('manage-items', App\User::class)
                                            <td>
                                                <div class="progress-info">
                                                    <div class="progress-percentage">
                                                        <span>{{ $item->current_user_items[0]->pivot->progress }}%</span>
                                                    </div>
                                                    @if ($item->current_user_items[0]->pivot->progress < 100)
                                                        <div class="progress-label">
                                                            <span>No terminado</span>
                                                        </div>
                                                    @else
                                                        <div class="progress-label">
                                                            <span>Terminado</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-info" role="progressbar"
                                                         aria-valuenow="{{ $item->current_user_items[0]->pivot->progress }}"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: {{ $item->current_user_items[0]->pivot->progress }}%;"></div>
                                                </div>
                                            </td>
                                        @endcannot

                                        <td>
                                            <img src="{{ $item->path() }}" alt="" style="max-width: 200px;">
                                        </td>
                                        @can('manage-items', App\User::class)
                                            <td>{{ $item->updated_at->format('d/m/Y H:i') }}</td>
                                        @endcan
                                        @can('manage-items', App\User::class)
                                            <td class="text-right">
                                                @if (auth()->user()->can('update', $item) || auth()->user()->can('delete', $item))
                                                    <div class="dropdown">
                                                        <a class="btn btn-sm btn-icon-only text-light" href="#"
                                                           role="button" data-toggle="dropdown" aria-haspopup="true"
                                                           aria-expanded="false">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div
                                                            class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            @can('update', $item)
                                                                <a class="dropdown-item"
                                                                   href="{{ route('item.edit', $item) }}">{{ __('Editar') }}</a>
                                                                <a class="dropdown-item"
                                                                   href="/transcription/{{$item->id}}">{{ __('Transcripcion') }}</a>
                                                            @endcan
                                                            @can('delete', $item)
                                                                <form action="{{ route('item.destroy', $item) }}"
                                                                      method="post">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="button" class="dropdown-item"
                                                                            onclick="confirm('{{ __("¿Estás seguro de que quieres eliminar este curso?") }}') ? this.parentElement.submit() : ''">
                                                                        {{ __('Eliminar') }}
                                                                    </button>
                                                                </form>
                                                            @endcan
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @include('layouts.footers.auth')
        </div>

    @else
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <h1 style="font-size: 3rem">Módulos</h1>
                </div>
            </div>
            <div style="display: none">{{$i = 0}}</div>
            <div class="row">
                @foreach($items as $item)
                    @if($i <= 0)
                        <div class="col-sm-6" style="margin-top: 5%">
                            <a href="item/present/{{ $item->id }}">
                                <img src="{{ $item->path() }}" alt="" width="90%">
                            </a>
                            <br>
                            <h2 style="margin-top: 2%">{{$item->name}}
                                @if ($item->current_user_items[0]->pivot->progress < 100)
                                    <span>{{ $item->current_user_items[0]->pivot->progress }}%</span>
                                    <div class="progress-label">
                                        <span>No terminado</span>
                                    </div>
                                @else
                                    <div class="progress-label">
                                        <span>Terminado</span>
                                    </div>
                                @endif

                            </h2>
                        </div>
                    @endif
                        <div style="display: none">{{$i = $i + 1}}</div>
                @endforeach
            </div>

        </div>

    @endif


    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
