@extends('layouts.app', [
    'title' => __('Calificaciones'),
    'parentSection' => 'grades-management',
    'elementName' => 'grades-management'
])

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Estadisticas') }}
            @endslot
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ __('Calificaciones usuarios') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Lista') }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">

                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Estadisticas') }}</h3>
                                <p class="text-sm mb-0">
                                    {{ __('Aquí puedes ver tus estadisticas.') }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div style="width: 18rem;">
                                <a href="{{ route('item.index') }}">
                                    <div class="card card-stats mb-4 mb-lg-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Numero de cursos activos</h5>
                                                    <span class="h2 font-weight-bold mb-0">1</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-xofluza text-white rounded-circle shadow">
                                                        <i class="fas fa-chart-bar"></i>
                                                    </div>
                                                    <br><br>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div style="width: 18rem;">
                                <a href="{{ route('user.index') }}">
                                    <div class="card card-stats mb-4 mb-lg-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Numero de alumnos</h5>
                                                    <span class="h2 font-weight-bold mb-0">{{$users_count}}</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-xofluza text-white rounded-circle shadow">
                                                        <i class="fas fa-user"></i>
                                                    </div>
                                                    <br><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div style="width: 18rem;">
                                <a href="{{ route('grades.index') }}">
                                    <div class="card card-stats mb-4 mb-lg-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Numero de certificados</h5>
                                                    <span class="h2 font-weight-bold mb-0">1</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-xofluza text-white rounded-circle shadow">
                                                        <i class="fas fa-chart-bar">{{$terminados}}</i>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                    <br><br><br><br><br>



                </div>

            </div>

        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
