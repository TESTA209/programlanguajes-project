# Proyecto de multiprocesamiento basado en hilos para materia de Lenguajes de programacion


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
PHP 7.2.5 o superior

Composer (artisan)

Laravel 8.0 o superior

NodeJS (npm)
```

### Instalación 🔧


```
Descargar el repo de git e ir a la carpeta correspondiente
```
```
composer install
```
```
Llenar el archivo .env con tus datos locales
```
```
php artisan key:generate
```
```
php artisan migrate --seed
```
```
php artisan link:storage
```
```
npm install
```


## Ejecutando las pruebas ⚙️

_Pruebas automatizadas para este sistema_


## Despliegue 📦

_Despliegue_

## Construido con 🛠️

* [Laravel](https://laravel.com/) - Laravel 5.5
* [Bootsrap](https://getbootstrap.com/) - Bootstrap 4.4.1
* [PHPSTORM](https://www.jetbrains.com/phpstorm/) - PHPStorm 2019.3


## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Josr Carlos Pacheco Sanchez** - *Trabajo Inicial* - [JoseCarosPa](https://github.com/JoseCarlosPa)


## Licencia 📄

Este proyecto está bajo la Licencia GNU GENERAL PUBLIC LICENSE V.3 - mira el archivo [LICENSE.md](LICENSE) para detalles


